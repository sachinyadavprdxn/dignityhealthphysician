# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/jobblaster/"
css_dir = "styles"
sass_dir = "_sass"
images_dir = "images"
javascripts_dir = "scripts"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :compact

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false

preferred_syntax = :sass

sass_options = {:debug_info=>false} # by Compass.app 


output_style = :expanded # by Compass.app 
line_comments = true # by Compass.app 