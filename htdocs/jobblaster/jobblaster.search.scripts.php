<?php
/**
 * Dev Note: load this file AFTER jquery.
 */
?>
<link rel="stylesheet" type="text/javascript" src="../../assets/vendor/datatables/css/dh_datatable.css">
<script type="text/javascript" src="../../jobblaster/scripts/json3.min.js"></script>
<script type="text/javascript" src="../../jobblaster/scripts/jquery-cookie.js"></script>
<script type="text/javascript" src="../../jobblaster/scripts/geoPosition.js"></script>
<script type="text/javascript" src="../../jobblaster/scripts/jb-gmaps.js"></script>
<script type="text/javascript" src="../../assets/js/jobblaster.js"></script> <!-- move this into jobblaster directory -->

<!-- Callback in the src attribute of the script below was causing a fatal error in IE7. Also see jb-gmaps.js. //-->
<!-- <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCCEMRN6u7YmeC9OMNLSzuCbV13u-u9xr4&sensor=true&callback=initialize_map"></script> -->
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCCEMRN6u7YmeC9OMNLSzuCbV13u-u9xr4&sensor=true&callback="></script>

<script>
$(document).ready(function(){
	//initialize the tabs
	init_tabs();
	set_tabs_click();

	//check for coordinates
	get_coords();

	//intialize datatable
	init_results_table();

	//handle crossbrowser issues with placeholder attribute.
	init_text_placeholder();

	//add functionality to 'clear' button.
	$("#jb-search-clear-btn").click(function(){clear_search_form();});
});
</script>

<?php
if(isset($hideApplyBtn) && ($hideApplyBtn == true)){
	echo "<script>$(document).ready(function(){ $('div.taleo-link').css('display','block'); });</script>";
}
?>
