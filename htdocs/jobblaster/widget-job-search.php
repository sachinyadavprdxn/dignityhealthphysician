<?php
 /**
 *	Job Search Widget
 *
 *	This is the job search form and includes the search results table and
 *	the associated google maps.
 *
 *	Dependency: jobblaster.include.php.
 */

	//Using $site_name variable to decide if user is on the system page, or in a facility page,
	//then changing the path of the needed files using $path_fix as declared in jobblaster.include.php.
	//if(isset($site_name) && $site_name !=""){
	//	require_once "../jobblaster/jobblaster.include.php";
	//}else{
	require_once $_SERVER["DOCUMENT_ROOT"] . "dignityhealthcareers/htdocs/jobblaster/jobblaster.include.php";
	//}
?>
<!-- <<<<<<<<<<<<<<<<<<<< JOBBLASTER STARTS HERE >>>>>>>>>>>>>>>>>>>>>>>> -->
<link rel="stylesheet" href="../../assets/vendor/datatables/css/dh_datatable.css" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="../../jobblaster/styles/jb.css" media="all" />

<!--[if lte IE 8]>
	<link rel="stylesheet" type="text/css" href="/jobblaster/styles/ie7.css" media="all" />
<![endif]-->

<?php
	//Job categories for the job categories drop down.
	getCategories();

	//DH facilities for the facilities drop down.
	getFacilities();

	//DH shifts for the shifts drop down.
	getShifts();

	//Query jobblaster for jobs based on GET request, if any.
	getJobs();

	//Get the closest facilities for the map. If no location is provided then default to SF.
	getFacilitiesNearMe();
?>
<div class="jb-container">
	<?php
		//
		//NOTE: This is for toggling a maintenance message at the top of the form.
		//
		//date_default_timezone_set("America/Los_Angeles");
		//$start_date = strtotime("2014-05-09 20:00:00");
		//$end_date = strtotime("2014-05-10 19:00:00");
		//$current_datetime = date("U");

		$hideApplyBtn = false;
		$hideMessage = true;

		//if($current_datetime > $start_date && $current_datetime < $end_date){ $hideApplyBtn = true;}
		//if($current_datetime > $end_date){ $hideMessage = true;	}

		if(!$hideMessage){
	?>
	<div class="maintenance-notice-wrapper">
		<div class="maintenance-notice">
			<p>Beginning at 8pm on Friday, May 9, Dignity Health will be undergoing site maintenance to improve our candidate experience and you will temporarily be <span class="underline">unable to apply online</span>. Please check back sometime after Saturday, May 10, at 7pm. Thank you.</p>
		</div>
	</div>
	<?php
		}
	?>
	<form id="jb-form" method="get" class="jb-search-form-widget">
            <input type="hidden" name="do" value="search" />
            <div class="jb-row">
                <div class="jb-form-col">
                    <input id="txt-keywords" name="keyword" type="text" placeholder="" value="<?php if(isset($_GET["keyword"]) && $_GET["keyword"]!= ""){ echo str_replace('"', '', trim($_GET["keyword"])); } ?>" class="placeholderinput" />
                    <label id="txt-keywords-label" for="txt-keywords" class="hidden-label" style="display:none;">Job title, keywords or description</label>
                </div>
                <div class="jb-form-col col-last">
               <div id="quality-option-value" class="job-search-option-value"></div>   
		    <select id="sel-category" name="category">
			<option value="">Career Area of Interest</option>
			<?php echo $category_options; ?>
		    </select>
                </div>
            </div> <!-- /.jb-row -->

            <div class="jb-row">
                <div class="jb-form-col">
                <?php
                	$selected_distance;
                	if(isset($_GET["distance"])){
                		$selected_distance = $_GET["distance"];
                	}else{
                		$selected_distance = "10000";
                	}
                ?>
                  <div id="distance-option-value" class="job-search-option-value"></div>
		    <select id="sel-distance" name="distance">
			<option value="20"<?php if($selected_distance == "20"){echo " selected";} ?>>Within 20 miles of</option>
			<option value="50"<?php if($selected_distance == "50"){echo " selected";} ?>>Within 50 miles of</option>
			<option value="80"<?php if($selected_distance == "80"){echo " selected";} ?>>Within 80 miles of</option>
			<option value="100"<?php if($selected_distance == "100"){echo " selected";} ?>>Within 100 miles of</option>
			<option value="200"<?php if($selected_distance == "200"){echo " selected";} ?>>Within 200 miles of</option>
			<option value="10000"<?php if($selected_distance == "10000"){echo " selected";} ?>>Anywhere</option>
		    </select>
                </div>
                <div class="jb-form-col col-last">
                    <!-- <input id="txt-citystate" name="location" type="text" placeholder="City &amp; State or Zip" value="<?php //if($currentCity!="" && $currentState!=""){echo($currentCity . ", " . $currentState);} ?>" /> //-->

                    <input id="txt-citystate" name="location" type="text" placeholder="" value="<?php if(isset($_GET["location"]) && $_GET["location"] != ""){ echo $_GET["location"]; } ?>" class="placeholderinput" />
                    <label id="txt-citystate-label" for="txt-citystate" class="hidden-label" style="display:none;">City &amp; State or Zip</label>
                </div>
            </div> <!-- /.jb-row -->

            <div class="jb-row">

                <div class="jb-form-col">
                  <div id="facilities-option-value" class="job-search-option-value"></div>
		    		<select id="sel-location" name="facility">
						<option value="">All Facilities</option>
						<?php echo $facility_options; ?>
		   			</select>
                </div>

                <div class="jb-form-col col-last">
                  <div id="shifts-option-value" class="job-search-option-value"></div>
                    <select id="sel-shift" name="shift">
						<option value="">All Shifts</option>
                        <?php echo $shift_options; ?>
		    		</select>
                </div>

            </div> <!-- /.jb-row -->

            <div class="jb-row">
                <div class="jb-form-col">
                    <!--<div class="employee-check"><input type="checkbox" value="true" name="employee"><span>I am an existing Dignity Health employee</span></div>-->
                </div>
                <div class="jb-form-col col-last">
                    <input type="submit" value="SUBMIT &gt;" id="jb-search-submit-btn" />
                    <a href="javascript:void(0);" id="jb-search-clear-btn">Reset search options</a>
                </div>
            </div> <!-- /.jb-row -->
	</form>

	<!-- begin tabs -->
	<div class="tabs-wrapper"><a name="main"></a>
	    <div class="tabs-wrapper-inner" style="">
			<ul class="tabs">
			    <li><a href="#" rel="listing-tab" class="active">Job Listing</a></li>
			    <li><a href="#" rel="map-tab" class="">Locations Near Me</a></li>
			</ul>
	    </div>
<!--
	    <div class="geolocation-wrapper">
	    <img src="/images/ico-geo.gif" />
	    <div class="location">Facilities Nearest:<br/><span class="client-location"><?php //echo $currentLocation; ?></span></div>
	    <div class="location-change"><a href="#" class="change-location-btn">Change Location</a></div>
	    <div class="location-change-form-wrapper">
	    <form action="" method="get">
	    <input type="hidden" name="do" value="changeloc" />
	    <input type="text" name="location" id="txt-location" value="" placeholder="zip code" />
	    <input type="submit" value="change" class="change-loc-submit-btn" />
	    <input type="button" value="cancel" class="change-loc-cancel-btn" />
	    <center>or</center>
	    <input type="button" value="Use my location" class="use-my-location-btn" />
	    </form>
	    </div>
	    </div>
//-->
	    <!-- /div -->
	    <div class="tab-content-wrapper">
		<!-- begin job listing table -->
		<div class="listing-tab tab-content">
		    <table cellspacing="0" cellpadding="0" class="jobs-listed display" id="jb-search-results"></table>
		</div>
		<!-- /end job listing table -->
		<div class="map-tab tab-content">
		    <!-- begin Google map -->
		    <div id="jb-map"></div>
		    <!-- end Google map -->
		</div>
	    </div>
	</div>
	<!-- /end tabs -->
</div>
<script>
var tblDataCols = [
	{
		"sTitle":"Distance",
		"mData": "distance",
		"sWidth": 0,
		"sClass": "null"
	},
	{
		"sTitle": "Posting Title",
		"mData" : "title",
		"sWidth": "50%",
		"sClass": "job-title"
	},
	{
		"sTitle": "Location",
		"mData" : "location",
		"sWidth": "35%",
		"sClass": "job-loc"
	},
	{
		"sTitle": "Shift",
		"mData" : "shift",
		"sWidth": "15%",
		"sClass": "job-shift"
	}];

var tblDataRows= [<?php echo $jobsForTable; ?>];
var totalJobs = "<?php echo number_format($countOfMatchedJobs); ?>";
var locations = { <?php echo '"values":['. $jobsForMap .']'; ?> };
var strFacility = "<?php echo $site_name; ?>";

<?php //echo "var locations = {\"values\":[". $jobsForMap ."]};";?>
</script>
<!-- <<<<<<<<<<<<<<<<<<<< JOBBLASTER ENDS HERE >>>>>>>>>>>>>>>>>>>>>>>>>> -->
