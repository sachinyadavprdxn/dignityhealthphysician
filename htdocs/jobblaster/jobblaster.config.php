<?php

/**
* These are general configuration settings for the jobblaster widgets. The most important
* setting here is that for the API_URL, which is the URL of the API.
*
*/

define("DEBUG", 	    		false);
define("CLIENT", 				"Dignity Health");
define("SITE_URL", 				"http://jb.dignityhealthcareers.org/");
define("SYSTEM_SITE_URL", 		"http://www.dignityhealthcareers.org");
define("GOOGLE_ANALYTICS_NUM",	"UA-9106857-1");

define("API_URL", 				"http://jobblaster02.jwtbase.com/api/dignity/v1");	//production
//define("API_URL", 				"http://lax3web10.jwtbase.com/api/dignity/v1");	//dev

define("DATA_FORMAT",			"json");
define("COOKIE_NAME", 			"dhjb");

define("DEFAULT_CITY",			"San Francisco");
define("DEFAULT_STATE",			"CA");
define("DEFAULT_LAT", 			"37.776787");
define("DEFAULT_LON", 			"-122.391858");

define("RADIUS", 				"10000");
define("DEFAULT_CATEGORY", 		"Accounting / Finance");
define("LIMIT", 				"150");

define("INFO_LOCATION_MESSAGE", "No location detected"); //Error message that prints above the table in the "change location" area.
define("INFO_LOCATION_ERROR",   "Location not found."); //Error message that prints in the table if no jobs are found.

?>