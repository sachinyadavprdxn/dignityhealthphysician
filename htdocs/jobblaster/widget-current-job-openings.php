<?php
/**
 *	Current Job Openings Widget
 *
 * 	The current job openings widget is for the sidebar area on the job
 * 	description pages. Currently it's only being pulled into job.php,
 * 	but can be included into any page as long as the dependency is fulfilled.
 *  	It requires that a default "job category" be set in the config file (see
 * 	jobblaster.config.php) and/or the variable $currentCategory be set in the
 * 	page in which this is being included.
 *
 *	Dependency: jobblaster.include.php.
*/

$path_fix = "";
if(isset($site_name) && $site_name !=""){
	$path_fix = "../";
}

include_once $path_fix . "dignityhealthcareers/htdocs/jobblaster/jobblaster.include.php";
if(!isset($currentCategory)){ $currentCategory = DEFAULT_CATEGORY; }
?>
<!-- Current Openings widget -->
<div class="current-openings-widget">
    <?php printCurrentJobs($currentCategory, 3); ?>
    <ul>
	<li class="widget-title">Current Openings</li>
	<li>
	    <ul class="job-listing">
	    <?php echo $jobsReturned; ?>
	    </ul>
	</li>
	<li><a href="index.php?do=search&category=<?php echo rawurlencode($currentCategory); ?>" class="widget-more-link">More Job Openings &gt;</a></li>
    </ul>
</div>
<!-- /Current Openings Widget -->
