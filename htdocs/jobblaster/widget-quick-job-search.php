<?php
 /**
 *	Quick Job Search Widget
 *
 * 	The quick job search widget is for the sidebar area on the job
 * 	description pages. Currently it's only being pulled into job.php,
 * 	but can be included into any page as long as the dependency is fulfilled.
 *
 *	Dependency: jobblaster.include.php.
 */

 $path_fix = "";
if(isset($site_name) && $site_name !=""){ $path_fix = "../"; }

include_once $path_fix . "dignityhealthcareers/htdocs/jobblaster/jobblaster.include.php";

//get an HTML string of all the job categories.
getCategories();
?>
<!-- quick search widget -->
    <div class="quick-search-widget">
	<p class="widget-title">Quick Job Search</p>
	<div class="widget-inner">
	    <form action="index.php#main" method="get" class="jb-search-form-widget">
		<select id="sel-careerarea" name="category">
		    <option value="">Career Area of Interest</option>
		    <?php echo $category_options; ?>
		</select>
		<input type="text" id="txt-location" name="location" value="" placeholder="" class="placeholderinput" />
		<label id="txt-location-label" for="txt-location" class="hidden-label" style="display:none;">City &amp; State or Zip</label>
		<input type="submit" value="SUBMIT &gt;" class="quick-search-widget-submit" />
	    </form>
	</div>
    </div>
  	<script type="text/javascript" src="<?php echo $path_fix; ?>js/jobblaster-widgets.js"></script>
  	<script type="text/javascript" src="<?php echo $path_fix; ?>js/jbtracking.js"></script>
  	<script>
  		jQuery(document).ready(function(){init_text_placeholder();});
  	</script>
<!-- /end quick search wiget -->
