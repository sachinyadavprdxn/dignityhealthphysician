<?php
/**
* JobBlaster Class
*
* Object that handles all the calls made to the jobblaster API. This
* requires configuration settings set in jobblaster.config.php. It's
* important to note that this uses cURL to make calls to the API.
*
* @version 1.0
* @author Ronan <ronan.mccoy@jwt.com>
*/

Class JobBlaster{
	protected $showDebug = false;

	protected $apiUrl;
	protected $dataFormat;
	protected $locCookie;
	protected $countReturnedJobs;
	protected $httpStatus;

	protected $city;
	protected $state;
	protected $longitude;
	protected $latitude;

	public function __construct(){
		$this->apiUrl 			= API_URL;
		$this->dataFormat 		= DATA_FORMAT;
		$this->locCookie 		= COOKIE_NAME;

		$this->countReturnedJobs= 0;
		$this->httpStatus 		= "";
	}

	public function __toString(){
		return __CLASS__;
	}

	public function getReturnedJobsCount(){
		return $this->countReturnedJobs;
	}

	public function getResponseStatus(){
		return $this->httpStatus;
	}

	public function setCity($val){
		$this->city = $val;
	}

	public function getCity(){
		return $this->city;
	}

	public function setState($val){
		$this->state = $val;
	}

	public function getState(){
		return $this->state;
	}

	public function setLatitude($val){
		$this->latitude = $val;
	}

	public function getLatitude(){
		return $this->latitude;
	}

	public function setLongitude($val){
		$this->longitude = $val;
	}

	public function getLongitude(){
		return $this->longitude;
	}

	public function getCategories(){
		$queryStr = "/category/?limit=1000&";
		return $this->queryJobBlasterAPI($queryStr);
	}

	public function getFacilities(){
		$queryStr = "/facility/?limit=1000&";
		return $this->queryJobBlasterAPI($queryStr);
	}

	public function getShifts(){
		$queryStr = "/shift/?";
		return $this->queryJobBlasterAPI($queryStr);
	}

	public function searchJobs($keywords, $radius, $location, $category, $subcat, $limit, $facility, $shift, $pageNumber, $isLatLong){
		$queryStr = "/jobsearch/?";

		if(trim($keywords) != ""){
			$queryStr .= "q=" . urlencode(trim($keywords)) . "&";
		}

		if(trim($radius) != ""){
			$queryStr .= "radius=" . trim($radius) . "&";
		}

		if(trim($location) != ""){
			if($isLatLong){
				$latLonArr = explode(",", $location);
				$queryStr .= "lat=". $latLonArr[0] ."&lon=". $latLonArr[1];
			}else{
				$queryStr .= "loc=" . urlencode(trim($location));
			}
			$queryStr .= "&";
		}

		//if string is comma delimited, then use category__in= instead of category=
		if(trim($category) != ""){
			$category_parts = explode(",", $category);
			if(count($category_parts) > 1){
				$queryStr .= "category__in=". urlencode(str_replace("/", "", trim($category))) ."&";
			}else{
				$queryStr .= "category=". urlencode(str_replace("/", "", trim($category))) ."&";
			}
		}

		if(trim($subcat) != ""){
			//$subcat_parts = explode(",", $subcat);
			//if(count($subcat_parts) > 1){
				$queryStr .= "subcategory__in=". urlencode($subcat) ."&";
			//}else{
			//	$queryStr .= "subcategory=". urlencode($subcat) ."&";
			//}
		}

		if(trim($limit) != ""){
			$queryStr .= "limit=" . trim($limit) . "&";
		}

		if(trim($facility) != ""){
			$queryStr .= "facility_exact__exact=" . urlencode(trim($facility)) . "&";
		}

		if(trim($shift) != ""){
			$queryStr .= "shift=". urlencode(trim($shift)) ."&";
		}

		if(trim($pageNumber) != ""){
			$queryStr .= "page=" . trim($pageNumber);
		}

		if($this->showDebug){
			echo "<div id='dev-info' style='display:block;border:1px solid yellow;padding:10px;color:#fff;background-color:#bcbcbc;'>jobblaster.class.php->seachJobs(): " . $this->apiUrl . $queryStr . "format=json</div>";
		}

		$queryResults = $this->queryJobBlasterAPI($queryStr);

		return $queryResults;
	}

	public function getJob($jobId){
		$query = "/job/" . $jobId . "/?";

		return $this->queryJobBlasterAPI($query);
	}

	public function getJobBySlug($jobSlug){
		$query = "/slug/" . $jobSlug . "/?";

		return $this->queryJobBlasterAPI($query);
	}

	public function getFacilitiesNearBy($usersLat, $usersLong, $usersCity, $usersState){

		$facilities_query = "/facilitysearch/?";
		$location_value = $usersCity .", ". $usersState;
		$facilities_query .= "loc=". urlencode($location_value) ."&limit=". LIMIT ."&";

		return $this->queryJobBlasterAPI($facilities_query);
	}

	protected function queryJobBlasterAPI($query){
		//$query should be something like "/[endpoint name]/?name=value&name2=value2".
		//So add both the '?' and the data-format.
		$apiRespDataFormat = "";
		$apiResponse = "";

		if(substr($query, -1) == "?" || substr($query, -1) == "&"){
			$apiRespDataFormat = "format=" . $this->dataFormat;
		}else{
			$apiRespDataFormat = "&format=" . $this->dataFormat;
		}

		$apiRequest = $this->apiUrl . $query . $apiRespDataFormat;

		$this->printInfo("API REQ: " . $apiRequest);

		$session = curl_init($apiRequest);

		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

		$apiResponse = curl_exec($session); // or die('Error: "' . curl_error($session) . '" - Code: ' . curl_errno($session));

		curl_close($session);

		//$this->printInfo("RESP: " . $apiResponse);

		return $apiResponse;
	}

	public function setLocationCookie($cityVal, $stateVal, $latitudeVal, $longitudeVal){
		//don't set cookie unless the user updated location using the form.
		$locationArr = array("latitude" => $latitudeVal, "longitude" => $longitudeVal,"city" => $cityVal, "state" => $stateVal);
		setcookie($this->locCookie, json_encode($locationArr));
	}

	public function readLocationCookie($paramToRead){
		$cookieVal = $_COOKIE[$this->locCookie];
		$cookieValAsJson = json_decode(str_replace("\\", "", $cookieVal), true);
		return $cookieValAsJson[$paramToRead];
	}

	public function updateLocationCookie($cityVal, $stateVal, $latitudeVal, $longitudeVal){
		if(isset($_COOKIE[$this->locCookie])){
			$this->setLocationCookie($cityVal, $stateVal, $latitudeVal, $longitudeVal);
		}
	}

	protected function printInfo($val){
		if($this->showDebug){
			echo "<div style='color:red;background:#ddd;padding:3px;border-bottom:1px dashed #eee;margin-bottom:5px;'>". $val ."</div>";
		}
	}
}
?>
