/**
API key:	
AIzaSyCCEMRN6u7YmeC9OMNLSzuCbV13u-u9xr4
Referers:	
Any referer allowed
Activated on:	Oct 8, 2013 4:37 PM
Activated by:	 ronan@ronanmccoy.com – you
**/

var _api = "AIzaSyCCEMRN6u7YmeC9OMNLSzuCbV13u-u9xr4";
var _canvas_id = "jb-map";
var _default_zoom = 6;

var _default_lat = defaultLat;
var _default_lon = defaultLon;

var _map_marker_icon = "../../jobblaster/images/dh_icon_sm.png"; //image dimensions should be 22x32. If different line 37 for dhPin.
_map_marker_icon = "../../jobblaster/images/dh_icon.png";	//this icon is 36x52. See line 38.

var dignityMap = null;
var marker = null;
var dhPin = null;
var infoWin = null;
var bounds = null;
var markers = [];
	
/**
* This function was the callback function for the google maps api. Currently not being called
* from the widget-job-search.php widget. But leaving here for future use should the need arise
* to dynamically update the map coordinates. See script tags in widget-job-search.php.
*/
function initialize_map(){

	var mapOptions = {
		zoom: _default_zoom,
		center: new google.maps.LatLng(_default_lat, _default_lon),
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DEFAULT}
	};
	
	dignityMap = new google.maps.Map(document.getElementById(_canvas_id), mapOptions);
	
	////dhPin = new google.maps.MarkerImage(_map_marker_icon, new google.maps.Size(22,32), new google.maps.Point(0,0), new google.maps.Point(18,42));
	dhPin = new google.maps.MarkerImage(_map_marker_icon, new google.maps.Size(36,52), new google.maps.Point(0,0), new google.maps.Point(18,42));
	
	infoWin = new google.maps.InfoWindow({size: new google.maps.Size(200,100)});
}

function center_gmap(usrObj){
	reset_gmap_coords(usrObj);
	
	dignityMap.setCenter(new google.maps.LatLng(_default_lat, _default_lon));
	dignityMap.setZoom(_default_zoom);
}

function reset_gmap_coords(usrObj){

	if(usrObj.latitude != "" && usrObj.longitude != ""){
		
		_default_lat = usrObj.latitude;
		_default_lon = usrObj.longitude;
		
		_default_zoom = 8;
	}
}

function create_marker(markerLat, markerLon, winContent){
	var infoHtml = winContent;
	var marker = new google.maps.Marker({
			position: new google.maps.LatLng(markerLat, markerLon),
			map: dignityMap,
			/*zIndex: Math.round(coords.lat() * -100001),*/
			icon: dhPin
		});
	google.maps.event.addListener(marker, "mouseover", function(){
			infoWin.setContent(infoHtml);
			infoWin.open(dignityMap, marker);
		});
	//bounds.extend(marker.position);
	
	markers.push(marker);
	
	return marker;
}

function remove_markers(){
	for (var i = 0; i < markers.length; i++ ) {
    	markers[i].setMap(null);
  	}
  	markers.length = 0;
}

function create_markers_for_map(markerLat, markerLon, windowHtml, bSetBound){	
	var custom_marker = create_marker(parseFloat(markerLat), parseFloat(markerLon), windowHtml);	
	if(bSetBound){bounds.extend(custom_marker.position);}
}

function load_map_locations(){
	var setBound = true;

	if(locations.values.length > 0){

		bounds = new google.maps.LatLngBounds();

		for(var i = 0; i < locations.values.length; i++){

			var facilityLat = locations.values[i].latitude;
			var facilityLong = locations.values[i].longitude;
			var facilityName = locations.values[i].facility;
			var facilityAddr = locations.values[i].address;
			var facilityCity = locations.values[i].city;
			var facilityState = locations.values[i].state;
			var facilityZip = locations.values[i].zip;
			
			var infowin_content = map_info_window_content(facilityName, facilityAddr, facilityCity, facilityState, facilityZip, facilityLat, facilityLong);

			if(i > 5){setBound = false;	}

			create_markers_for_map(facilityLat, facilityLong, infowin_content, setBound);
		}

		dignityMap.fitBounds(bounds);
	}
}

function map_info_window_content(facility, addr, city, state, zip, coordLat, coordLong){
	var winContent = "<div class='map-info-window'>";
	winContent += "<span class='facility'>"+ facility +"</span>";
	winContent += "<span class='street'>"+ addr +"</span>";
	winContent += "<span class='citystate'>"+ city + ", " + state + " " + zip +"</span>";
	winContent += "</div>";
	return winContent;
}

//window.onload = load_gmaps;
//google.maps.event.addDomListener(window, "load", initialize);