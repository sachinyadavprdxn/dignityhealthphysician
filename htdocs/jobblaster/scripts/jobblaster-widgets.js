/**
 * TODO: This file can be deleted. - RM. 4/7/2015
 */
var _debugOn = false;
var ApiUrl = "http://jobblaster02.jwtbase.com/api/dignity/v1"; //production
//var ApiUrl = "http://lax3web10.jwtbase.com/api/dignity/v1";
var baseJobUrl = "http://dignityhealth.org/careers/jobs";

var _currentFacilityUrl = "";

function ws_lookup(query, fCallback){
    if(query == "" || fCallback == ""){return null;}
    var apiCall = ApiUrl + query;

    $j.ajax({
        url: apiCall,
	type: "GET",
	dataType: "jsonp",
	jsonpCallback: fCallback,
	cache: true,
	success: function(data, textStatus, xhr){
        print_debug_info(textStatus, false);
	},
	error: function(xhr, textStatus, errorThrown){
	   print_debug_info(textStatus, true);
	}
    });
}

function get_categories(){
    var _q = "/category/?format=jsonp&limit=1000";
    ws_lookup(_q, "print_categories");
}

function get_current_openings(isPriority, subcategoryList, facilityName, facilityUrl){
    var _q = "/jobsearch/?format=jsonp&limit=3";

    if(facilityUrl != ""){
        _currentFacilityUrl = facilityUrl;
    }else{
        _currentFacilityUrl = "";
    }

    if(isPriority){
        _q += "&priority=T";
    }

    if(subcategoryList != "" && subcategoryList != "all"){
        //_q += "&category_exact__in=" + categoryList;
        _q += "&subcategory__in=" + subcategoryList;
    }

    if(facilityName != ""){
        _q += "&facility=" + facilityName
    }

    print_debug_info("Job openings looking: '"+ _q +"'", false);
    ws_lookup(_q, "print_current_openings");

}

/*
function get_current_openings(bPriority){
    var _q = "/jobsearch/?format=jsonp&limit=3";

    if(bPriority){ _q += "&priority=T";}

    ws_lookup(_q, "print_current_openings");
}
*/
/*
function get_current_openings_by_category(cat, bPriority){
    var _q = "/jobsearch/?format=jsonp&limit=3&category=" + cat;

    if(bPriority){ _q += "&priority=T";}

    ws_lookup(_q, "print_current_openings");
}
*/
/*
function get_current_openings_by_facility(facility, bPriority){
    var _q = "/jobsearch/?format=jsonp&limit=3&facility=" + facility;

    if(bPriority){ _q += "&priority=T";}

    ws_lookup(_q, "print_current_openings");
}
*/

function print_categories(wsdata){
    var i = 0;
    var j = wsdata.objects.length;
    var _options = "";
    if(j > 0){
        for(i=0; i < j; i++){
            _options += "<option value='"+ wsdata.objects[i].name +"'>"+ wsdata.objects[i].name +"</option>";
        }
        $j("#sel-careerarea").append(_options);
    }
}

function print_current_openings(wsdata){
    var i = 0;
    var j = wsdata.objects.length;
    var _html = "";
    if(j > 0){
        for(i = 0; i < j; i++){
            _html += "<li>";

            if(_currentFacilityUrl != ""){
                _html += "<span class='job-title'><a href='"+ baseJobUrl + "/" + _currentFacilityUrl +"/job.php?id="+ wsdata.objects[i].slug +"'>"+ wsdata.objects[i].title +"</a></span>";
            }else{
                _html += "<span class='job-title'><a href='"+ baseJobUrl +"/job.php?id="+ wsdata.objects[i].slug +"'>"+ wsdata.objects[i].title +"</a></span>";
            }

            _html += "<span class='job-shift'>" + wsdata.objects[i].employment_type + " / " + wsdata.objects[i].shift + "</span>";
            _html += wsdata.objects[i].facility + ",<br>"+ wsdata.objects[i].city + ", " + wsdata.objects[i].state
            _html += "</li>";
        }

        _html = "<ul class='job-listing'>" + _html + "</ul>";
        $j("div.current-openings-widget li.widget-jobs-listing").html(_html);

    }else{
        //_html = "<li>No matching jobs where found. Please click below to search for more jobs.</li>";
        //$j("div.current-openings-widget a.widget-more-link").text("Search Jobs");

        $j("div.current-openings-widget").css("display","none");
    }
}

function html5AttributeSupport(theElement, theAttribute){
    var _testObject = document.createElement(theElement);
    var _isSupported = false;
    if(theAttribute in _testObject){
        _isSupported = true;
    }
    return _isSupported;
}

function init_text_placeholder(){
    if(html5AttributeSupport("input", "placeholder")){
        //if browser supports placeholder, then add the placeholder
        //value to the placeholder.
        $j("input.placeholderinput").each(function(){
            var el = $j(this);
            var elLabelId = el.attr("id") + "-label";
            var phValue = $j("#" + elLabelId).text();
            el.attr("placeholder", phValue);
        });
    }else{
        //if browser does not support placeholder (looking at you IE7!),
        //get the value of the placeholder, and put it in the value attribute.
        $j("input.placeholderinput").each(function(){
            var el = $j(this);
            var elLabelId = el.attr("id") + "-label";
            var phValue = $j("#" + elLabelId).text();

            if(el.attr("value") == ""){
                el.attr("value",phValue);
                el.css("color", "#999");
            }

            el.focus(function(){
                if(this.value == phValue){
                    this.value = "";
                }
            }).blur(function(){
                if(this.value ==""){
                    this.value = phValue;
                    el.css("color","#999");
                }
            });
        });

        //make sure that when the form submits, the placeholder values are removed.
        $j("form.jb-search-form-widget").submit(function(){
            $j("input.placeholderinput").each(function(){
                var el = $j(this);
                var elLabelId = el.attr("id") + "-label";
                var phValue = $j("#" + elLabelId).text();
                if(el.attr("value") == phValue){
                    el.attr("value", "");
                }
            });
        });
    }
}

function print_debug_info(message, isWarning){
    if(_debugOn && message != ""){
        if(window.console && console.log){
            if(isWarning){
                    console.log(message);
            }else{
                    console.info(message);
            }
        }else{
            alert(message);
        }
    }
}
