
var _debugOn = false;
var userLoc = new LocationObj();
var dataTable;

$(document).ready(function(){

	//initialize the tabs
	init_tabs();
	set_tabs_click();

	//set up location change form (i.e. for click to view the form).
	//location_change_init();

	//check for coordinates
	get_coords();

	//intialize datatable
	init_results_table();

	//handle crossbrowser issues with placeholder attribute.
	init_text_placeholder();

	//add functionality to 'clear' button.
	$("#jb-search-clear-btn").click(function(){
		clear_search_form();
	});
	
}); //end document ready

function clear_search_form(){
	$("#txt-keywords").val("");
	$("#txt-citystate").val("");
	$("#sel-distance").val("10000");
	$("#sel-category").val("");
	$("#sel-shift").val("");

	if(strFacility == ""){
		$("#sel-location").val("");
	}
}

function init_tabs(){
	var _rel = $("ul.tabs li a.active").attr("rel");
	$("." + _rel).show();
}

function set_tabs_click(){
	$("ul.tabs li a").click(function(){

		$(".active").removeClass("active");
		$(this).addClass("active");

		$("div.tab-content").hide();

		var _showTab = $(this).attr("rel");
		$("." + _showTab).show();

		if(_showTab == "map-tab"){
			initialize_map();
			load_map_locations();
		} //have to force reinitialization of google map.
		return false;
	});
}

/*
function location_change_init(){
	//onclick to open form.
	$(".change-location-btn").click(function(){
		$(".location").hide();
		$(".location-change").hide();

		$(".location-change-form-wrapper").css("display","inline-block");
		return false;
	});

	//onclick to close form.
	$(".change-loc-cancel-btn").click(function(){
		$(".location-change-form-wrapper").hide();

		$(".location").show();
		$(".location-change").show();
		return false;
	});

	//onclick of the submit button
	$(".change-loc-submit-btn").click(function(){
		set_current_position_zipcode($("#txt-zip").val());

		//close the form
		$(".location-change-form-wrapper").hide();
		$(".location").show();
		$(".location-change").show();
	});
	
	$("input.use-my-location-btn").click(function(){
		geo_lookup();
		
		$(".location-change-form-wrapper").hide();

		$(".location").show();
		$(".location-change").show();
		
		return false;
	});
}*/

function init_results_table(){
	/*docs: legacy.datatables.net*/
	dataTable = $("#jb-search-results").dataTable({
		"bAutoWidth": false,
		"bProcessing": false,
		"iDisplayLength": 10,
		"sPaginationType": "full_numbers",
		"oLanguage":{
			"oPaginate":{"sNext":"Next &gt;", "sPrevious": "&lt; Prev", "sFirst":"&lt;&lt;", "sLast":"&gt;&gt;"},
			"sInfo": "Showing _START_ to _END_ (" + totalJobs + " jobs found)",
			"sZeroRecords": "No matching positions were found."
		},
		"aoColumnDefs": [{"bVisible":false,"aTargets":[0]}],
		"bFilter": false,
		"bLengthChange": false,
		"bRetrieve": true,
		"aaData": tblDataRows,
		"aoColumns": tblDataCols
	});
	dataTable.fnSort([[0,'asc']]);

	if(dataTable.fnGetData().length == 0){
		//console.log("no data found, hide paging and info.");
		$(".dataTables_paginate").css("display", "none");
		$("#jb-search-results_info").css("display", "none");
	}
	//"sInfo": "Showing _START_ to _END_ of _TOTAL_ positions." - prints to the bottom of the table "showing 0-xx of xxx positions."
	$("#jb-search-results_paginate").after("<div class='about-search-results'>Only the first 150 search results are shown. You can refine your search by adding keywords or using the filters.</div>");
}

/*
function re_init_results_table(){
	dataTable.fnDraw();
}
*/

function re_init_results_table(jsonData){
	dataTable.fnClearTable();
	dataTable.fnAddData(jsonData);
	dataTable.fnDraw();
}

function get_coords(){
	if (typeof read_location_cookie() === "undefined" || read_location_cookie() == ""){
		if(queryStringParser("do") != "search"){geo_lookup();}
	}else{
		//If cookie exists, server-side should take care of it.
		print_debug_info("Cookie exists, load jobs from server-side for "+ read_location_cookie() +".", false);
		if(queryStringParser("do") != "search"){
			var posit = jQuery.parseJSON(read_location_cookie());
			set_form_inputs(posit.city, posit.state, ApiRadius);
		}
	}
}

function geo_lookup(){
	//initialize default location in case user refuses to share location or 
	//browser does not support geolocation.
	userLoc.city = defaultCity;
	userLoc.state = defaultState;

	//request location if browser can handle it.
	if(geoPosition.init()){
		geoPosition.getCurrentPosition(set_current_position, geo_lookup_error, {enableHighAccuracy: true});
	}
}

function geo_lookup_error(lookupErr){
	//this really does nothing. We could set the default location here, but that's already done.
	//If debugging, print out info here.
	print_debug_info("Unable to query user location. Message returned as ["+ lookupErr +"].", true);
	print_debug_info("Using default location of: " + dCity + ", " + dState, false);
}

function set_current_position(positObj){
	//call webservice to get closest city-state combo, & jobs.
	var _query = "/jobsearch/?format=jsonp&lat="+ positObj.coords.latitude + "&lon=" + positObj.coords.longitude + "&limit="+ ApiLimit +"&radius=" + ApiRadius;
	print_debug_info("Query from javascript (set_current_position): " + _query, false);
	ws_lookup(_query, "job_search_results");
}

function job_search_results(wsData){

	//set_form_inputs(wsData.meta.city, wsData.meta.state, wsData.meta.radius);

	print_debug_info("loading from javascript", false);
	print_debug_info("location retrieved from API is: "+ wsData.meta.city + ", " + wsData.meta.state, false);

	//$("#search-results").find("tr:gt(0)").remove();

	var objectCount = wsData.objects.length;
	var tableRows = "";

	for(var i = 0; i < objectCount; i++){
		var rows = '{';
		if(wsData.objects[i].distance){
			rows += '"distance":"'+wsData.objects[i].distance+'",';
		}else{
			rows += '"distance":"NULL",';
		}
		rows += '"title":"<a href=\'job.php?id='+wsData.objects[i].slug+'\' class=\'job-link\'>'+ RemoveUnsupportedStrings(wsData.objects[i].title) +'</a>",';
		rows += '"location":"<a href=\'job.php?id='+wsData.objects[i].slug+'\' class=\'job-link\'>'+ wsData.objects[i].facility + '<br>' + wsData.objects[i].city + ", " + wsData.objects[i].state +'</a>",';
		rows += '"shift":"<a href=\'job.php?id='+wsData.objects[i].slug+'\' class=\'job-link\'>'+wsData.objects[i].shift+'</a>"';
		rows += '},';
		tableRows += rows;
	}

	tableRows = '[' + removeTrailingComma(tableRows) +']';
	print_debug_info("Table Rows: " + tableRows, false); 
	
	//reinitialize the table sorting/filter functionality.
	//re_init_results_table();
	re_init_results_table(JSON.parse(tableRows));

	set_user_location(wsData.meta.city, wsData.meta.state, wsData.meta.lat, wsData.meta.lon);

	get_facilities_nearby(wsData.meta.city, wsData.meta.state);

	//document.location.reload(true);
}

/*function job_search_results_pagereload(wsData){
	print_debug_info("loading from javascript, but causing a page reload.", false);
	print_debug_info("location retrieved from API is: "+ wsData.meta.city + ", " + wsData.meta.state, false);

	set_user_location(wsData.meta.city, wsData.meta.state, wsData.meta.lat, wsData.meta.lon);

	document.location.reload(true);
}*/

function set_user_location(_city, _state, _latitude, _longitude){
	userLoc.city = _city;
	userLoc.state = _state;
	userLoc.latitude = _latitude;
	userLoc.longitude = _longitude;
	
	set_location_cookie(userLoc.toString());

	set_form_inputs(_city, _state, ApiRadius);
}

function set_form_inputs(_city, _state, _radius){

	print_debug_info("Setting location and radius to:"+ _city +","+_state+" at radius " + _radius, false);

	$("#txt-citystate").val(_city + ", " + _state);
	
	if($.isNumeric(_radius)){
		if(parseInt(_radius) > 200){
			$("#sel-distance").val("10000");
		}else{
			$("#sel-distance").val(_radius);
		}
	}
}

function get_facilities_nearby(_city, _state){
	var _query = "/facilitysearch/?format=jsonp&loc=" + _city + "," + _state + "&limit=200";
	ws_lookup(_query, "facilities_nearby");
}

function facilities_nearby(wsData){
	if(wsData.objects.length > 0){

		var string_to_json = "";

		for(var j=0; j < wsData.objects.length; j++){
			string_to_json += '{"facility":"'+ wsData.objects[j].name +'","longitude":"'+ wsData.objects[j].location[0] +'","latitude":"'+ wsData.objects[j].location[1] +'","city":"'+ wsData.objects[j].city +'","state":"'+ wsData.objects[j].state +'","address":"'+ wsData.objects[j].address +'","zip":"'+ wsData.objects[j].zip_code +'"},';
		}

		//remove trailing comma.
		//string_to_json = string_to_json.replace(/,+$/, "");
		string_to_json = removeTrailingComma(string_to_json);

		//convert to json.
		string_to_json = '"values":['+ string_to_json +']';
		locations = JSON.parse('{'+string_to_json+'}');

		remove_markers();
		//load_map_locations();
	}
}

function read_location_cookie(){
	return $.cookie(LocationCookieName);
}

function set_location_cookie(cookieData){
	if(cookieData != ""){
		$.cookie(LocationCookieName, cookieData);
	}
}

function queryStringParser(p){
	var vars = [], hash;
	var q = document.URL.split('?')[1];
	if(q != undefined){
		q = q.split('&');
		for(var i = 0; i < q.length; i++){
			hash = q[i].split('=');
			vars.push(hash[1]);
			vars[hash[0]] = hash[1];
		}
	}
	return vars[p];
}

function print_debug_info(message, isWarning){
	if(_debugOn && message != ""){
		if(window.console && console.log){
			if(isWarning){
				console.log(message);
			}else{
				console.info(message);
			}
		}else{
			alert(message);
		}
	}
}

function ws_lookup(query, fCallback){
	if(query == "" || fCallback == ""){return null;}

	var apiCall = ApiUrl + query;

	print_debug_info("API call from ws_lookup function: '"+ apiCall +"'", false)

	$.ajax({
		url: apiCall,
		type: "GET",
		dataType: "jsonp",
		jsonpCallback: fCallback,
		cache: true,
		success: function(data, textStatus, xhr){
			print_debug_info(textStatus, false);
		},
		error: function(xhr, textStatus, errorThrown){
			print_debug_info(textStatus, true);
		}
	});
}

function LocationObj(){
	this.longitude = "";
	this.latitude = "";
	this.city = "";
	this.state = "";
	this.toJson = function(){
		var _json = {"latitude": this.latitude, "longitude" : this.longitude, "city": this.city, "state": this.state};
		return _json;
	}
	this.toString = function(){
		return JSON.stringify(this.toJson());
	}
}

function removeTrailingComma(val){
	var retString = "";
	if(val != ""){
		retString = val.replace(/,+$/, "");
	}
	return retString;
}

function html5AttributeSupport(theElement, theAttribute){
	var _testObject = document.createElement(theElement);
	var _isSupported = false;
	if(theAttribute in _testObject){
		_isSupported = true;
	}
	return _isSupported;
}

function init_text_placeholder(){
	if(html5AttributeSupport("input", "placeholder")){
		//if browser supports placeholder, then add the placeholder
		//value to the placeholder.
		$("input.placeholderinput").each(function(){
			var el = $(this);
			var elLabelId = el.attr("id") + "-label";
			var phValue = $("#" + elLabelId).text();
			el.attr("placeholder", phValue);
		});
	}else{
		//if browser does not support placeholder (looking at you IE7!),
		//get the value of the placeholder, and put it in the value attribute.
		$("input.placeholderinput").each(function(){
			var el = $(this);
			var elLabelId = el.attr("id") + "-label";
			var phValue = $("#" + elLabelId).text();

			if(el.attr("value") == ""){
				el.attr("value",phValue);
				el.css("color", "#999");
			}

			el.focus(function(){
				if(this.value == phValue){
					this.value = "";
				}
			}).blur(function(){
				if(this.value ==""){
					this.value = phValue;
					el.css("color","#999");
				}
			});
		});

		//make sure that when the form submits, the placeholder values are removed.
		$("form.jb-search-form-widget").submit(function(){
			$("input.placeholderinput").each(function(){
				var el = $(this);
				var elLabelId = el.attr("id") + "-label";
				var phValue = $("#" + elLabelId).text();
				if(el.attr("value") == phValue){
					el.attr("value", "");
				}
			});
		});
	}
}

function RemoveUnsupportedStrings(uncleanStr){
	var cleanStr = uncleanStr;

	cleanStr.replace("\\\\", "&#92;&#92;");
	cleanStr.replace("\\", "&#92;");
	return cleanStr;
}