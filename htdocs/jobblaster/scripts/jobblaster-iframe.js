/**
* Forces iFrame to be the height of it's content.
* See http://www.456bereastreet.com/archive/201112/how_to_adjust_an_iframe_elements_height_to_fit_its_content/
*/
document.domain = "dignityhealthcareers.org";
window.onload = function () {
	iFrameObj = document.getElementById('jobblasterIframe');
	//get all querystring params and use it to update the iframe source.
	//Update: turned off. Using idoc script to pull the qs.
	//setIframeSource(iFrameObj);

	//ID is of iframe in facility site job search pages.
    setIframeHeight(iFrameObj);
};
function setIframeHeight(iframe) {
	if (iframe) {
        var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
        if (iframeWin.document.body) {
            iframe.height = iframeWin.document.documentElement.scrollHeight || iframeWin.document.body.scrollHeight;
        }
    }
};
function setIframeSource(iframe){
	var c_loc = window.location.toString();
	qs = c_loc.split("?")[1];
	iframe.src = iframe.src + "?" + qs;
}