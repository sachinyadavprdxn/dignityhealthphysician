// jbtracking.js
//
// v.0.1 -- First try at code -- $() for jq needed to be re-written as $j()
//
// var prodId = getParameterByName('prodId');
// 
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function appendToApply(parameter) {
    var tracker = getParameterByName(parameter);
    if (typeof tracker === 'undefined') {
      // variable is undefined
    } else if (tracker == '') {
      // It's blank, so don't do it.
    } else if (tracker == null) {
      // It's null, so don't do it.
    } else {
	
	  // Need to rewrite to loop thru different classes that might be correct
	
      // If we do have a job code, then attach it to the a href with class jb_apply
      $j('a.jb_apply').attr('href',function(i,str) {
        return str + '&' + parameter + '=' + tracker;
      });

      // If we do have a job code, then attach it to the a href with class jb-apply
      $j('a.jb-apply').attr('href',function(i,str) {
        return str + '&' + parameter + '=' + tracker;
      });

    }
}


$j( document ).ready(function() {
	appendToApply('media');
	appendToApply('source');
});
