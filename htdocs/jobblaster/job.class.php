<?php
/**
* Job Class
*
* Object to store properties of an individual job.
*
* @version 1.0
* @author Ronan <ronan.mccoy@jwt.com>
*/

class Job{
	protected $showDebug = false;
	protected $title;
	protected $id;
	protected $category;
	protected $city;
	protected $stateAbbrev;
	
	public function __construct(){
		$this->title 		= "";
		$this->id 		= "";
		$this->category 	= "";
		$this->city 		= "";
		$this->stateAbbrev 	= "";

		$this->printInfo(__CLASS__ ." object created. Line ". __LINE__.".");
	}

	public function __toString(){
		return __CLASS__;
	}

	public function setTitle($val){
		$this->title = $val;
	}

	public function getTitle(){
		return $this->title;
	}

	public function setJobId($val){
		$this->id = $val;
	}

	public function getJobId(){
		return $this->id;
	}

	public function setCategory($val){
		$this->category = $val;
	}

	public function getCategory(){
		return $this->category;
	}

	public function setCity($val){
		$this->city = $val;
	}

	public function getCity(){
		return $this->city;
	}

	public function setState($val){
		$this->stateAbbrev = $val;
	}

	public function getState(){
		return $this->stateAbbrev;
	}

	public function getCityAndState(){
		return ucwords($this->city) . ", ". strtoupper($this->stateAbbrev);
	}

	protected function printInfo($val){
		if($this->showDebug){
			echo "<div style='color:red;background:#ddd;padding:3px;width:30%;'>". $val ."<br>". __FILE__ .".</div>";
		}
	}
}

?>