<?php
/**
* This file pulls in all the resources needed for the job search
* functionality on a given page. It also sets up the javascript variables
* for the geo-location functionality.
* Note: requires cURL to make calls to API.
*
* @version 1.2
* @author Ronan <ronan.mccoy@jwt.com>
*/

//$path_fix = "";
//if(isset($site_name) && $site_name !=""){ $path_fix = "../"; }

// Default configuration for JobBlaster.
require_once $_SERVER["DOCUMENT_ROOT"]  . "dignityhealthcareers/htdocs/jobblaster/jobblaster.config.php";

// Class to handle calls to the JobBlaster API.
require_once $_SERVER["DOCUMENT_ROOT"]  . "dignityhealthcareers/htdocs/jobblaster/jobblaster.class.php";

// Class to hold details of a given job.
require_once $_SERVER["DOCUMENT_ROOT"] . "dignityhealthcareers/htdocs/jobblaster/job.class.php";

//Print out global javascript values.
echo "<script>\n";
echo "var LocationCookieName	= '". COOKIE_NAME ."';\n";
echo "var ApiUrl				= '". API_URL ."';\n";
echo "var ApiLimit				= ". LIMIT .";\n";
echo "var ApiRadius             = ". RADIUS .";\n";
echo "var UserLocationInfo		= '". INFO_LOCATION_MESSAGE ."';\n";
echo "var defaultCity           = '". DEFAULT_CITY ."';\n";
echo "var defaultState 			= '". DEFAULT_STATE ."';\n";
echo "var defaultLat			= ". DEFAULT_LAT .";\n";
echo "var defaultLon			= ". DEFAULT_LON .";\n";
echo "</script>\n";

//global variables to hold jobs lists.
$jobsReturned = "";
$jobsForMap = "";
$jobsForTable = "";
$job_data = "";
$countOfMatchedJobs = "0"; //the value returned from the API in [meta][total_count].

$currentCity = "";
$currentState = "";
$currentLat = "";
$currentLon = "";
$currentLocation = "";

//global variables for dropdown options.
$facility_options = "";
$category_options = "";
$shift_options = "";

//the work-horse.
$jb = new JobBlaster;

function getCategories(){
	global $jb, $category_options;
	$selectedCategory = "";

	$categoriesJson = $jb->getCategories();
	$categories = json_decode($categoriesJson, true);
	$categories_arr = $categories["objects"];

	if(isset($_GET["category"]) && $_GET["category"] != ""){
		if(strtolower($_GET["category"]) != "all"){
			$selectedCategory = trim($_GET["category"]);
		}
	}

	foreach($categories_arr as $category){
		if($selectedCategory == $category["name"]){
			$category_options .= "<option value='". $category["name"] ."' selected>". $category["name"] ."</option>";
		}else{
			$category_options .= "<option value='". $category["name"] ."'>". $category["name"] ."</option>";
		}
	}
}

function getFacilities(){
	global $jb, $facility_options, $site_name;
	$selectedFacility = "";

	$facilitiesJson = $jb->getFacilities();
	$facilities = json_decode($facilitiesJson, true);
	$facilities_arr = $facilities["objects"];

	if(isset($site_name) && $site_name != ""){
		if(!isset($_GET["facility"])){
			$selectedFacility = $site_name;
		}
	}

	if(isset($_GET["facility"]) && $_GET["facility"] != ""){
		$selectedFacility = trim($_GET["facility"]);
	}

	foreach($facilities_arr as $facility){
		if($selectedFacility == $facility["name"]){
			$facility_options .= "<option value=\"". $facility["name"] ."\" selected>". $facility["name"] ."</option>";
		}else{
			$facility_options .= "<option value=\"". $facility["name"] ."\">". $facility["name"] ."</option>";
		}
	}
}


function getShifts(){
	global $jb, $shift_options;
	$selectedShift = "";

	$shiftJson = $jb->getShifts();
	$shifts = json_decode($shiftJson,true);
	$shifts_arr = $shifts["objects"];

	if(isset($_GET["shift"]) && $_GET["shift"] != ""){
		$selectedShift = trim($_GET["shift"]);
	}

	foreach($shifts_arr as $shift){
		if($selectedShift == $shift["name"]){
			$shift_options .= "<option value='". $shift["name"] ."' selected>". $shift["name"] ."</option>";
		}else{
			$shift_options .= "<option value='". $shift["name"] ."'>". $shift["name"] ."</option>";
		}
	}
}


function getJobs(){
	global $jb, $currentCity, $currentState, $currentLat, $currentLon, $currentLocation, $site_name;

	$keywords = "";
	$category = "";
	$subcat = "";
	$limit = LIMIT;
	$radius = RADIUS;
	$isLatLon = false;
	$facility = "";
	$shift = "";

	if(isset($_GET["location"]) && $_GET["location"] != ""){
		$currentLocation = trim($_GET["location"]);
	}else{
		if(!isset($_GET["do"])){
			if(isset($_COOKIE[COOKIE_NAME]) && $_COOKIE[COOKIE_NAME] != ""){
				$currentLocation = $jb->readLocationCookie("city") . "," . $jb->readLocationCookie("state");
			}else{
				$currentLocation = DEFAULT_CITY . "," . DEFAULT_STATE;
			}
		}
	}

	//echo $currentLocation;

	//check for values should this be a search form submission.
	if(isset($_GET["keyword"]) && $_GET["keyword"] != ""){
		$keywords = str_replace('"', '', trim($_GET["keyword"]));
	}

	if(isset($_GET["distance"]) && $_GET["distance"] != "" && is_numeric($_GET["distance"])){
		$radius = trim($_GET["distance"]);
	}

	if(isset($_GET["category"]) && $_GET["category"] != ""){
		$category = trim($_GET["category"]);
		if(strtolower($category) == "all"){ $category = "";}
	}

	if(isset($_GET["subcat"]) && $_GET["subcat"] != ""){
		$subcat = trim($_GET["subcat"]);
		if(strtolower($subcat) == "all"){ $subcat = "";}
	}

	if(isset($site_name) && $site_name != ""){
		if(!isset($_GET["facility"])){
			$facility = $site_name;
		}
	}

	if(isset($_GET["facility"]) && $_GET["facility"] != ""){
		$facility = trim($_GET["facility"]);
	}

	if(isset($_GET["shift"]) && $_GET["shift"] != ""){
		$shift = trim($_GET["shift"]);
	}

	$searchResults = $jb->searchJobs($keywords, $radius, $currentLocation, $category, $subcat, $limit, $facility, $shift, "", $isLatLon);

	printJobsToScreen($searchResults);

}

function getJobsInCategory($category, $limit, $site_name){
	global $jb;
	$shift = "";

	if($category == ""){
		$category = DEFAULT_CATEGORY;
	}else{
		$category = str_replace("/", "", $category);
	}

	if($limit == ""){
		$limit = LIMIT;
	}

	if(isset($_COOKIE[COOKIE_NAME]) && $_COOKIE[COOKIE_NAME]!= ""){

		$currentCity = $jb->readLocationCookie("city");
		$currentState = $jb->readLocationCookie("state");

	}else{

		$currentCity = DEFAULT_CITY;
		$currentState = DEFAULT_STATE;
	}
	$currentLocation = $currentCity . ", " . $currentState;
	$facility = "";

	$searchResults = $jb->searchJobs("", RADIUS, $currentLocation, $category, "", $limit, $site_name, $shift, "", false);

	return $searchResults;
}

function getAllJobs(){
	global $jb;
	$searchResults = $jb->searchJobs("", "", "", "", "", "2000", "", "", "", "");

	$allJobs = json_decode($searchResults,true);

	$meta = $allJobs["meta"];

	if(array_key_exists("error",$meta)){
		echo "There was an error. Unable to retrieve jobs.";
	}else{

		$listOfJobs = $allJobs["objects"];
		foreach($listOfJobs as $jobItem){
			echo "<a href='job.php?id=". $jobItem["slug"]."&media=rep'>". $jobItem["title"] ."</a>";
			echo "<br>";
		}
	}

}

function getJobDetails($id){
	global $jb, $job_data;

	//$detailsAsJson = $jb->getJob($id);
	$detailsAsJson = $jb->getJobBySlug($id);

	$job_data = json_decode($detailsAsJson,true);
}

function getFacilitiesNearMe(){
	global $jb;
	global $jobsForMap;

	$myLong = "";
	$myLat = "";
	$myCity = "";
	$myState = "";

	if(isset($_COOKIE[COOKIE_NAME]) && $_COOKIE[COOKIE_NAME] != ""){
		$myLong = $jb->readLocationCookie("longitude");
		$myLat = $jb->readLocationCookie("latitude");
		$myCity = $jb->readLocationCookie("city");
		$myState = $jb->readLocationCookie("state");
	}else{
		$myCity = DEFAULT_CITY;
		$myState = DEFAULT_STATE;
	}

	$mylocations = $jb->getFacilitiesNearBy($myLong, $myLat, $myCity, $myState);

	$data = json_decode($mylocations, true);

	$meta = $data["meta"];

	if(array_key_exists("error",$meta)){
		//$currentLocation = INFO_LOCATION_ERROR;
	}else{

		$jobsForMap = "";

		$mapLocations = $data["objects"];

		foreach($mapLocations as $location){
			$jobsForMap .= '{"facility":"'. $location['name'] .'","longitude":'. $location['location'][0] .',"latitude":'. $location['location'][1] .',"city":"'. $location['city'] .'","state":"'. $location['state'] .'","address":"'. $location['address'] .'","zip":"'. $location['zip_code'] .'"},';
			//echo $location['name'] . '<br><br>';
		}

		//remove trailing ',' from json.
		//$jobsForMap = substr($jobsForMap, 0, -1);
		$jobsForMap = utilRemoveTrailingComma($jobsForMap);
	}
} //end getFacilitiesNearMe

function printJobsToScreen($jsonString){
	global $jb, $currentCity, $currentState, $jobsForTable;
	global $currentLat, $currentLat, $currentLon, $currentLocation;
	global $countOfMatchedJobs;
	$jobsForTable = "";

	$data = json_decode($jsonString, true);

	$meta = $data["meta"];

	if(array_key_exists("error",$meta)){
		$currentLocation = INFO_LOCATION_ERROR;

		echo "No data returned.";
		echo $meta["error"];
	}else{
		if(array_key_exists("city",$meta) && array_key_exists("state",$meta) && $meta["city"] != "" && $meta["state"] != ""){

			$currentCity = $meta["city"];
			$currentState = $meta["state"];
			$currentLat = $meta["lat"];
			$currentLon = $meta["lon"];

			$currentLocation = $currentCity . ", " . $currentState;
		}else{
			$currentLocation = INFO_LOCATION_ERROR;
		}

		if(array_key_exists("total_count", $meta)){
			$countOfMatchedJobs = $meta["total_count"];
		}

		//load jobs
		$jobs = $data["objects"];
		foreach($jobs as $job){
			$jobsForTable .= '{';
			if(array_key_exists('distance', $job)){
				$jobsForTable .= '"distance":"'. $job["distance"].'",';
			}else{
				$jobsForTable .= '"distance":"",';
			}
			$jobsForTable .= '"title":"<a href=\"job.php?id='.$job["slug"].'\" class=\"job-link\">'.$job["title"].'</a>",';
			$jobsForTable .= '"location":"<a href=\"job.php?id='.$job["slug"].'\" class=\"job-link\">'.$job["facility"].'<br>'. $job["city"] .', '. $job["state"] .'</a>",';
			$jobsForTable .= '"shift":"<a href=\"job.php?id='.$job["slug"].'\" class=\"job-link\">'.$job["shift"].'</a>"';
			$jobsForTable .= '},';
		}
		$jobsForTable = utilRemoveTrailingComma($jobsForTable);
	}
}

function printCurrentJobs($category, $limit){
	global $jobsReturned, $site_name;

	$jsonString = getJobsInCategory($category, $limit, $site_name);

	$data = json_decode($jsonString,true);

	$jobs = $data["objects"];

	foreach($jobs as $job){
		$jobsReturned .= "<li>";
		$jobsReturned .= "<span class='job-title'><a href='job.php?id=". $job["slug"] ."'>". $job["title"] ."</a></span>";
		$jobsReturned .= "<span class='job-shift'>". $job["employment_type"] . " / " . $job["shift"] ."</span>";
		$jobsReturned .= $job["facility"] . ",<br>" . $job["city"] . ", " . $job["state"];
		$jobsReturned .= "</li>";
	}
}

function utilRemoveTrailingComma($stringVal){
	$returnValue = "";
	if($stringVal != ""){
		$returnValue = substr($stringVal, 0, -1);
	}
	return $returnValue;
}

?>
