<?php
/**
*	Job Details Widget
*
* 	Requests the details of the job given a valid job id number in the
*	querystring. if job details are returned, the appropriate job
*	category is set.
*
* 	Dependency: jobblaster.include.php.
*/
?>

<!-- Begin job details widget -->
<div class="back-to-search"><a href="#" onClick="window.history.go(-1);return false;">&larr; Back to Job Search</a></div>
<?php
include_once $_SERVER["DOCUMENT_ROOT"] . "dignityhealthcareers/htdocs/jobblaster/jobblaster.include.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "dignityhealthcareers/htdocs/assets/vendor/Mobile-Detect-2.8.11/Mobile_Detect.php";

$currentCategory = "";

if(isset($_GET["id"])){

    getJobDetails($_GET["id"]);

    if($job_data["slug"] == ""){
    //if($job_data["title"] == ""){

	//assumption made here that if no data is returned for the job, then it has expired. Even if it's an
	//invalid search, prompt the user to try and search again.
	echo "<br /><br /><p>The position you are looking for has already expired. Please try another search.</p>";

    }else{

	$currentCategory = $job_data["category"];
?>
	<div class="dh-addthis-widget">
	    <!-- AddThis Button BEGIN -->
	    <div class="addthis_toolbox addthis_default_style " style="min-width:84px;">
	    <a class="addthis_button_preferred_1"></a>
	    <a class="addthis_button_preferred_2"></a>
	    <!--<a class="addthis_button_preferred_3"></a>-->
	    <!--<a class="addthis_button_preferred_4"></a>-->
	    <a class="addthis_button_linkedin"></a>
	    <a class="addthis_button_compact"></a>
	    <!--a class="addthis_counter addthis_bubble_style"></a //-->
	    </div>
	    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52a7cc9c20178455"></script>
	    <!-- AddThis Button END -->
	</div>
	<br style="clear:both;">

	<h2><?php echo $job_data["title"]; ?></h2>

	<div class="job-details-info">
		<?php
			if($job_data["facility"]["ats_name"] != ""){ echo "<p><span>LOCATION:</span>&nbsp;".$job_data["facility"]["ats_name"]."</p>";}
			if($job_data["department"] != ""){ echo "<p><span>DEPARTMENT:</span>&nbsp;".$job_data["department"]."</p>";}
			if($job_data["work_schedule"] != "" && $job_data["shift"]){ echo "<p><span>SHIFT:</span>&nbsp;".$job_data["work_schedule"] .", " . $job_data["shift"]."</p>";}
			if($job_data["employment_type"] != ""){ echo "<p><span>EMPLOYMENT TYPE:</span>&nbsp;". $job_data["employment_type"] ."</p>";}
			if($job_data["hours_per_period"] != ""){ echo "<p><span>HRS PER PAY PERIOD:</span>&nbsp;".$job_data["hours_per_period"]."</p>";}
			if(isset($_GET["media"]) && $_GET["media"] == "rep"){
				echo "<p><span>ORGANIZATION:</span>&nbsp;". $job_data["organization"] ."</p>";
				echo "<p><span>CATEGORY:</span>&nbsp;". $job_data["category"] ."</p>";
			}
			if($job_data["job_id"] != ""){ echo "<p><span>JOB ID:</span>&nbsp;".$job_data["job_id"]."</p>";}
		?>
		<!--
      	<p><span>LOCATION:</span> Glendale Mem Hosp Hlth Ctr, Glendale, CA 91204</p>
      	<p><span>DEPARTMENT:</span> </p>
      	<p><span>SHIFT:</span> 12HOUR, Day</p>
      	<p><span>EMPLOYMENT TYPE:</span> Full Time</p>
      	<p><span>HRS PER PAY PERIOD:</span> 72</p>
      	<p><span>JOB ID:</span> 1400024307</p>
      	-->
	</div>

	<?php echo $job_data["description"]; ?>

	<h3 class="jb-about-us">About Us</h3>
	<?php echo $job_data["about_us"]; ?>

<?php
	date_default_timezone_set("America/Los_Angeles");
	$start_date = strtotime("2014-05-09 20:00:00");
	$end_date = strtotime("2014-05-10 19:00:00");
	$current_datetime = date("U");

	$hideApplyBtn = false;
	$hideMessage = true;

	//if($current_datetime > $start_date && $current_datetime < $end_date){ $hideApplyBtn = true; }

	//if($current_datetime > $end_date){ $hideMessage = true;	}

	if(!$hideMessage){
?>
		<div class="maintenance-notice-wrapper">
			<div class="maintenance-notice">
				<p>Beginning at 8pm on Friday, May 9, Dignity Health will be undergoing site maintenance to improve our candidate experience and you will temporarily be <span class="underline">unable to apply online</span>. Please check back sometime after Saturday, May 10, at 7pm. Thank you.</p>
			</div>
		</div>
<?php
	}
	if(!$hideApplyBtn){
		$detect = new Mobile_Detect;
		if ($detect->isMobile() || $detect->isTablet() ) {
			$apply_url = "https://dignityhealth.staging.jibeapply.com/login?jobId=" . $job_data["job_id"];
		} else {
			$apply_url = $job_data["apply_url"];
		}
?>
		<p><br><a href="<?php echo $apply_url; ?>" target="_blank" class="jb-apply">Apply Now</a></p>
<?php
	}
}
}else{
  echo "<h1>Error</h1><p>Invalid request. Please try searching again.</p>";
}
?>
<script>
	var pageCat = '<?php echo $currentCategory; ?>';
	var facilityName = '';
	var facilityId = '';
</script>
<!-- End job details widget -->
