<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
?>
<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<!--[if IE 8]> <html class="ie8"> <![endif]-->
<html lang="en">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <base href=".">

  <title>Dignity Health Careers</title>

  <!-- 120 word description for SEO purposes goes here. Note: Usage of lang tag. -->
  <meta name="description" content="Dignity Health Home | Excellent care, delivered with compassion, for all in need. Hello humankindness. (415) 438-5500">

  <!-- Keywords to help with SEO go here. Note: Usage of lang tag.  -->
  <meta name="keywords" content="dignity health, dignity health hospitals, dignity health doctors, hello human kindness, dignity health care, health care">

  <!-- View-port Basics: http://mzl.la/VYREaP -->
    <!-- This meta tag is used for mobile device to display the page without any zooming,
         so how much the device is able to fit on the screen is what's shown initially.
         Remove comments from this tag, when you want to apply media queries to the website. -->
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <!-- To adhear no-cache for Chrome -->
         <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
         <meta http-equiv="Pragma" content="no-store, no-cache" />
         <meta http-equiv="Expires" content="0" />
         <meta name="author" content="Dignity Health">
         <meta name="generator" content="Direct Control">
         <meta name="MSSmartTagsPreventParsing" content="true">
         <meta charset="utf-8">
         <meta name="format-detection" content="telephone=no">

         <!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
         <link rel="shortcut icon" href="../../favicon.ico">

         <!-- Default style-sheet is for 'media' type screen (color computer display).  -->
         <link rel="stylesheet" media="screen" href="../../assets/css/style.css" >
         <!--    html5shiv aka html5 shim. Supporting HTML5 and CSS for IE browsers less than IE9. -->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body class="nursing job-description search-more-jobs">
    <div class="main-container">
      <div class="nav">
        <div class="nav-search-mob">
          <a href="../Search-Jobs/index.html" class="search-job ui-link" title="Search jobs">
            Search jobs
          </a>
        </div>
        <ul class="mob-nav">
          <li class="ourLocations navLoc menuItem">
            <a href="../Explore-Dignity-Locations/index.html" class="ui-link ourLocations" title="Our Locations">Our Locations</a>
          </li>
          <li class="ourmission navLoc menuItem">
            <a href="../Our-Mission-Values-and-Vision/index.html" class="ui-link ourmission" title=" Mission, Vision &amp; Values"> Mission, Vision &amp; Values</a>
          </li>
          <li class="ourcareerpath navLoc menuItem">
            <a href="../Careers-Paths/index.html" class="ui-link ourcareerpath" title="Career Paths">Career Paths</a>
          </li>
          <li><a href="../Students-and-Recent-Grads/index.html">Students &amp; Recent Grads</a></li>
          <li><a href="../Total-Rewards/index.html">Total Rewards</a></li>
          <li><a href="../Event-Calendar/index.html">Events Calendar</a></li>
          <li><a href="../Meet-Our-People/index.html">Meet our people</a></li>
          <li><a href="../Fellowship/index.html">Administrative fellowship</a></li>
          <li><a href="../FAQs/index.html">Faq's</a></li>
          <li class="mobileMenuItem parentMenu"><a href="http://dignityhealth.org" target="_blank">Dignityhealth.org</a></li>
          <li class="mobileMenuItem parentMenu"><a href="https://hellohumankindness.org" target="_blank" class="humankindness">Hello humankindness</a></li>
          <li class="mobileMenuItem parentMenu"><a href="http://www.dignityhealth.org/cm/content/ways-to-give.asp" target="_blank">Ways to give</a></li>
          <li id="mobileMenuSpecials" class="mobileMenuItem parentMenu"><a  href="https://www.dignityhealth.org/Who_We_Are/Contact_Us/index.htm" target="_blank">Contact us</a></li>
          <li class="mobileMenuItem parentMenu"><a href="http://www.dignityhealth.org/cm/content/about-us.asp" target="_blank">About us</a></li>
        </ul>
      </div>

      <div class="container">

        <!--  Header starts here -->
        <header id="headerContainer">
          <div id="topNav" class="topnav cf">
            <ul id="topMenuLeft">
              <li>
                <a href="https://hellohumankindness.org" target="_blank" class="ui-link" title="Hello humankindness">Hello humankindness.</a>
              </li>
            </ul>
            <div id="menutop">
              <ul>
                <li>
                  <a href="http://www.dignityhealth.org/cm/content/ways-to-give.asp" target="_blank" class="ui-link" title="Ways To Give">Ways to give</a>
                </li>
                <li>
                  <a href="https://www.dignityhealth.org/Who_We_Are/Contact_Us/index.htm" target="_blank" class="ui-link" title="Contact Us">Contact us</a>
                </li>
                <li>
                  <a href="http://www.dignityhealth.org/cm/content/about-us.asp" target="_blank" class="ui-link" title="About Us">About us</a>
                </li>
              </ul>
            </div>
          </div>
          <div id="centerHeaderMenuContainer">
            <div class="mobileMenuContainer">
              <a href="fixme"></a>
            </div>
            <h1 class="logoContainer" id="logo">
              <a href="../../index.html" class="ui-link" title="Dignity Health">Dignity</a>
            </h1>
            <nav class="giantLinks">
              <ul>
                <li class="ourLocations navLoc menuItem">
                  <a href="../Explore-Dignity-Locations/index.html" class="ui-link ourLocations" title="Our Locations">Our locations</a>
                </li>
                <li class="ourmission navLoc menuItem">
                  <a href="../Our-Mission-Values-and-Vision/index.html" class="ui-link ourmission" title=" Mission, Vision &amp; Values"> Mission, Vision &amp; Values</a>
                </li>
                <li class="ourcareerpath navLoc menuItem">
                  <a href="../Careers-Paths/index.html" class="ui-link ourcareerpath" title="Career Paths">Career paths</a>
                </li>
              </ul>
            </nav>
            <div class="nav-search">
              <a id="bigButtonSearchTool" href="../Search-Jobs/index.html" class="search-job ui-link" title="Search Jobs">
                Search jobs
              </a>
            </div>
          </div>
        </header>


        <!-- Content starts here -->
        <div id="mainContent">
          <div id="pullDropDownContainer" class="cf">
            <div id="pullDropDownTitle">
              <span class="dropName">Careers</span>
              <div class="learnmore-careers">
                <span class="dropExplore">Learn More About Careers</span>
              </div>
            </div>
            <div id="dropDownContent">
              <div class="DropDownFacilityContainer">
                <a href="../Students-and-Recent-Grads/index.html" class="ui-link">
                  <img src="../../assets/images/sub-nav-students-and-grads.jpg" height="112" width="242" alt="students and recent grads"/>
                  <span class="select">students and recent grads</span>
                </a>
              </div>
              <div class="DropDownFacilityContainer">
                <a href="../Total-Rewards/index.html" class="ui-link">
                  <img src="../../assets/images/sub-nav-total-rewards.jpg" height="112" width="242" alt="total rewards"/>
                  <span>total rewards</span>
                </a>
              </div>
              <div class="DropDownFacilityContainer">
                <a href="../Event-Calendar/index.html" class="ui-link">
                  <img src="../../assets/images/sub-nav-events-calendar.jpg" height="112" width="242" alt="events Calendar"/>
                  <span>events Calendar</span>
                </a>
              </div>
              <div class="DropDownFacilityContainer">
                <a href="../Meet-Our-People/index.html" class="ui-link">
                  <img src="../../assets/images/sub-nav-meet-our-people.jpg" height="112" width="242" alt="meet our people"/>
                  <span>meet our people</span>
                </a>
              </div>
              <div class="DropDownFacilityContainer">
                <a href="../Fellowship/index.html" class="ui-link">
                  <img src="../../assets/images/image-1.jpg" height="112" width="242" alt="administrative fellowship"/>
                  <span>administrative fellowship</span>
                </a>
              </div>
              <div class="DropDownFacilityContainer">
                <a href="../FAQs/index.html" class="ui-link">
                  <img src="../../assets/images/sub-nav-FAQ.jpg" height="112" width="242" alt="FAQ’S"/>
                  <span>FAQ’S</span>
                </a>
              </div>
            </div>
          </div>
          <!--   Main body starts here -->
          <div class="mainBody">
            <div class="banner">
              <img src="../../assets/images/job-description_header.jpg"  width="1700" alt="job-description" />
            </div>

            <!-- Wrapper -->
            <div class="wrapper">

              <div class="jobsearch-discover cf">

                <div class="nursing-specialities">
                  <div class="specialities-outline">
                    <?php
                    include_once $_SERVER["DOCUMENT_ROOT"] . "dignityhealthcareers/htdocs/jobblaster/widget-job-details.php";
                    ?>
                  </div>
                </div>

                <div class="right-sidebar">
                  <div class="job-search">

                    <h3><span></span>Quick Job Search</h3>
                    <form action="index.php" method="get" class="jb-search-form-widget">
                      <input type="hidden" value="search" name="do" />

                      <label for="sel-careerarea">Career Area of Interest</label>
                      <div class="select-wrap">
                        <div id="selected-option-value" class="selected-option-value"></div>
                        <select id="sel-careerarea" name="category">
                          <option value="">Specialty Area</option>
                        </select>
                      </div>


                      <label for="txt-location">City &amp; State or Zip Code</label>
                      <input type="text" id="txt-location" name="location" value="" placeholder="Enter City &amp; State or Zip Code" />

                      <input type="submit" value="submit" class="submit" />
                    </form>

                  </div>
                  <div class="current-openings-widget">
                    <h3 class="widget-title">Current Openings</h3>
                    <div class="job-opening">
                    <!--
                      <div>
                        <h4><a href="fixme">Registered Nurse - ICU FT VARIED</a></h4>
                        <span>Full Time/ Varied</span>
                        <span>Mercy Hospital of Bakersfield and</span>
                        <span>Mercy Southwest</span>
                        <span>Bakersfield, CA</span>
                      </div>
                      <div>
                        <h4><a href="fixme">Sub Acute Registered Nurse/ Per Diem/ Nights</a></h4>
                        <span>Per Diem/ Nights</span>
                        <span>California Hospital Medical Center</span>
                        <span>Los Angeles, CA</span>
                      </div>
                      <div>
                        <h4><a href="fixme">Clinical Informatics Nurse - FT DAY</a></h4>
                        <span>Full Time/ Day</span>
                        <span>St. Bemardine Medical Center</span>
                        <span>San Bemardino, CA</span>
                      </div>
                      <a href="http://dignityhealth.org/careers/jobs/?do=search&amp;subcat=Laboratory" class="widget-more-link" target="_blank">More Job Openings</a>
                    -->
                  </div>
                </div>
              </div>
            </div><!--  Jobsearch-discover ends -->
          </div>
        </div>
      </div>

      <!--    Footer starts here -->
      <footer>
        <div class="footerTable">
          <ul class="footerCell footercell-desktop">
            <li><a href="../Explore-Dignity-Locations/index.html" class="ui-link" title="Our Locations">our Locations</a></li>
            <li><a href="../Our-Mission-Values-and-Vision/index.html" class="ui-link" title="Mission, Vision &amp; Values">mission, vision &amp; values</a></li>
            <li><a href="../Careers-Paths/index.html" class="ui-link" title="Career Paths">career paths</a></li>
          </ul>
          <ul class="footerCell footercell-desktop">
            <li><a href="../Students-and-Recent-Grads/index.html" class="ui-link" title="Students &amp; Recent Grads">students &amp; recent grads</a></li>
            <li><a href="../Total-Rewards/index.html" class="ui-link" title="Total Rewards">total rewards</a></li>
            <li><a href="../Event-Calendar/index.html" class="ui-link" title="Events Calendar">events calendar</a></li>
          </ul>
          <ul class="footerCell footercell-desktop">
            <li><a href="../Meet-Our-People/index.html" class="ui-link" title="Meet Our People">meet our people</a>
            </li>
            <li><a href="../Fellowship/index.html" class="ui-link" title="Administrative Fellowship">administrative fellowship</a></li>
            <li><a href="../FAQs/index.html" class="ui-link" title="FAQ's">FAQ's</a></li>
          </ul>
          <ul class="footerCell footercell-desktop">
            <li><a href="http://dignityhealth.org" target="_blank" class="ui-link" title="Dignityhealth.org">Dignityhealth.org</a></li>
            <li><a href="https://hellohumankindness.org" target="_blank" class="ui-link" title="Hello humankindness">hello humankindness</a></li>
            <li><a href="http://www.dignityhealth.org/cm/content/ways-to-give.asp" target="_blank" class="ui-link" title="Ways To Give">ways to give</a></li>
            <li><a href="https://www.dignityhealth.org/Who_We_Are/Contact_Us/index.htm" target="_blank" class="ui-link" title="Contact Us">contact us</a></li>
            <li><a href="http://www.dignityhealth.org/Who_We_Are/index.htm" target="_blank" class="ui-link" title="About Us">about us</a></li>
          </ul>
          <div class="mobile-footer">
            <ul class="footerCell">
              <li><a href="../Explore-Dignity-Locations/index.html" class="ui-link" title="our Locations">our Locations</a></li>
              <li><a href="../Our-Mission-Values-and-Vision/index.html" class="ui-link" title="mission, vision &amp; values">mission, vision &amp; values</a></li>
              <li><a href="../Careers-Paths/index.html" class="ui-link" title="career paths">career paths</a></li>
              <li><a href="../Students-and-Recent-Grads/index.html" class="ui-link" title="students &amp; recent grads">students &amp; recent grads</a></li>
              <li><a href="../Total-Rewards/index.html" class="ui-link" title="total rewards">total rewards</a></li>
              <li><a href="../Event-Calendar/index.html" class="ui-link" title="events calendar">events calendar</a></li>
            </ul>
            <ul class="footerCell">
              <li><a href="../Meet-Our-People/index.html" class="ui-link" title="meet our people">meet our people</a></li>
              <li><a href="../Fellowship/index.html" class="ui-link" title="administrative fellowship">administrative fellowship</a></li>
              <li><a href="../FAQs/index.html" class="ui-link" title="FAQ's">FAQ's</a></li>
              <li><a href="http://dignityhealth.org" target="_blank" class="ui-link" title="Dignityhealth.org">Dignityhealth.org</a></li>
              <li><a href="https://hellohumankindness.org" target="_blank" class="ui-link" title="hello humankindness">hello humankindness</a></li>
              <li><a href="http://www.dignityhealth.org/cm/content/ways-to-give.asp" target="_blank" class="ui-link" title="ways to give">ways to give</a></li>
              <li><a href="https://www.dignityhealth.org/Who_We_Are/Contact_Us/index.htm" target="_blank" class="ui-link" title="contact us">contact us</a></li>
              <li><a href="http://www.dignityhealth.org/Who_We_Are/index.htm" target="_blank" class="ui-link" title="about us">about us</a></li>
            </ul>
          </div>
        </div>
        <div class="subFooterLinks ">
          <div class="footer-wrapper cf">
            <ul class="leftLinks">
             <li>
              <a href="http://terms.dignityhealth.org/cm/content.asp?pid=4&amp;lid=4&amp;facility_id=61&amp;anchor=boilerplate_groupID_1" target="_blank" class="ui-link" title="Legal &amp; Privacy Notices">legal &amp; privacy notices</a>
            </li>
            <li class="linkTwo">
              <a href="http://terms.dignityhealth.org/cm/content.asp?pid=4&lid=4&facility_id=61&anchor=bp_179" target="_blank" class="ui-link" title="Copyright">copyright</a>
            </li>
            <li>
              <a href="../../sitemap.html" class="ui-link lastFooterLink" title="Sitemap">sitemap</a>
            </li>
          </ul>
          <div class="sub-footer">
            <p class="followUsContainer">follow our careers conversation:</p>
            <ul class="rightLinks">
              <li><a href="https://twitter.com/DignityHlthJobs" target="_blank" class="ui-link" title="Twitter"><img src="../../assets/images/twitter.png" alt="twitter"></a></li>
              <li><a href="https://www.facebook.com/dignityhealthcareers" target="_blank" class="ui-link" title="Facebook"><img src="../../assets/images/facebook.png" alt="Facebook"></a></li>
              <li><a href="https://www.linkedin.com/company/dignity-health/careers?trk=top_nav_careers" target="_blank" class="ui-link" title="LinkedIn"><img src="../../assets/images/in.png" alt="LinkedIn"/></a></li>
              <li id="pinterest" class="lastFooterLink">
                <a href="http://www.pinterest.com/dignitycareers/" target="_blank" class="ui-link" title="Pinterest"><img src="../../assets/images/pin.png" alt="Pinterest"/></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

  </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="../../assets/js/jquery.flexslider-min.js"></script>
<script src="../../assets/js/jobblaster.js"></script>
<script src="../../assets/js/script.js"></script>
<script>
  $(document).ready(function(){
    get_categories();

    //value of current job category page the user is on.
      //This is only populated if the user is on a page
      //for a specific job type/role.
      var pageCat = '';

      //Name of the facility.
      //This is only populated if on a facility website.
      // e.g. "St. Joseph's Hospital and Medical Center"
      var facilityName = '';

      //A value to identify the facility (provided by CMS).
      //This is only populated if on a facility website.
      // e.g. "St-Josephs-Hospital-and-Medical-Center-AZ"
      var facility_id = '';


      //get the current openings.
      get_current_openings(true, pageCat, facilityName, facility_id);
    });
</script>
</body>
</html>


