/**
 * jb - Simple object to encapsulate calls to the jobblaster API.
 *      Includes a method to write debug messages to the console
 *      if supported by the browser and debug is set to 'true'.
 */
 var jb = function(){
  var config = {
    debug: true,
    api: "http://jobblaster02.jwtbase.com/api/dignity/v1",
    baseJobUrl: window.location.protocol + "//" + window.location.host + "/" + "dignityhealthcareers/htdocs/careers/jobs",
    currentFacilityUrl: ""
  }

  function ws_lookup(query, callBack){
    if(query == "" || callBack == ""){
      log("Error. Cannot query the API without proper query parameters nor a callback function.");
      return null;
    }

    log(config.api + query);

    $.ajax({
      url: config.api + query,
      type: "GET",
      dataType: "jsonp",
      jsonpCallback: callBack,
      cache: true,
      success: function(data, textStatus, xhr){ log(textStatus); },
      error: function(xhr, textStatus, errorThrown){ log(textStatus); log(errorThrown); }
    });
  }

  function log(_msg){
    if(config.debug){
      try{
        console.error(_msg);
      }catch(err){
        alert(_msg);
      }
    }
  }

  return{
    config: config,
    log: log,
    api: ws_lookup
  }
}();

var userLoc = new LocationObj();
var dataTable;

/**
 * get_current_openings    Builds a jobblaster api query based on the job category and facility
 *                         name provided. Uses 'isPriority' to sort. Makes a call to the API with
 *                         callback to function 'print_current_openings()' which builds, then
 *                         injects the HTML into the 'current openings' html list.
 *
 * @param  {Boolean} isPriority
 * @param  {String}  subcategoryList
 * @param  {String}  facilityName
 * @param  {String}  facilityUrl
 */
 function get_current_openings(isPriority, subcategoryList, facilityName, facilityUrl){
  var _q = "/jobsearch/?format=jsonp&limit=3";

  if(facilityUrl != ""){
    jb.config.currentFacilityUrl = facilityUrl;
  }

  if(isPriority){
    _q += "&priority=T";
  }

  if(subcategoryList != "" && subcategoryList != "all"){
    _q += "&subcategory__in=" + subcategoryList;
  }

  if(facilityName != ""){
    _q += "&facility=" + facilityName
  }

  jb.api(_q, "print_current_openings");
} //end get_current_openings()

/**
 * print_current_openings  Builds an HTML list, with the <ul> tag pair, from
 *                         the JSON parameter.
 * @param  {json} wsdata
 */
 function print_current_openings(wsdata){
  var i = 0;
  var j = wsdata.objects.length;
  var _html = "";
  if(j > 0){

    for(i = 0; i < j; i++){
      _html += "<div>";
      if(jb.config.currentFacilityUrl != ""){
        _html += "<h4><a href='"+ jb.config.baseJobUrl + "/" + jb.config.currentFacilityUrl +"/job.php?id="+ wsdata.objects[i].slug +"'>"+ wsdata.objects[i].title +"</a></h4>";
      }else{
        _html += "<h4><a href='"+ jb.config.baseJobUrl +"/job.php?id="+ wsdata.objects[i].slug +"'>"+ wsdata.objects[i].title +"</a></h4>";
      }
      _html += "<span>" + wsdata.objects[i].employment_type + " / " + wsdata.objects[i].shift + "</span>";
      _html += "<span>" + wsdata.objects[i].facility + "</span>";
      _html += "<span>" + wsdata.objects[i].city + ", " + wsdata.objects[i].state + "</span>";
      _html += "</div>";
    }

    _html += "<a href='"+ jb.config.baseJobUrl + "/?do=search&subcat=Laboratory' class='widget-more-link'>More Job Openings</a>";

    $("div.current-openings-widget div.job-opening").html(_html);

  }else{
        //found not jobs, so hide the widget.
        $("div.current-openings-widget").css("display","none");
      }
} // end print_current_openings()

/**
 * get_categories  Queries the jobblaster API to get a list
 *                 of job categories. Callback function then
 *                 builds an HTML string to populate the
 *                 'career area' dropdown.
 */
 function get_categories(){
  var _q = "/category/?format=jsonp&limit=1000";
  jb.api(_q, "print_categories");
} // end get_categories()

/**
 * print_categories Builds an HTML list of 'options' to populate
 *                  the 'career area' dropdown.
 * @param  {json} wsdata
 */
 function print_categories(wsdata){
  var i = 0;
  var j = wsdata.objects.length;
  var _options = "";
  if(j > 0){
    for(i=0; i < j; i++){
      _options += "<option value='"+ wsdata.objects[i].name +"'>"+ wsdata.objects[i].name +"</option>";
    }
    $("#sel-careerarea").append(_options);
  }
} // end print_categories()

/**
 * [LocationObj description]
 */
 function LocationObj(){
  this.longitude = "";
  this.latitude = "";
  this.city = "";
  this.state = "";
  this.toJson = function(){
    var _json = {"latitude": this.latitude, "longitude" : this.longitude, "city": this.city, "state": this.state};
    return _json;
  }
  this.toString = function(){return JSON.stringify(this.toJson()); }
}

/* -------------------------------------------------------
*   Functions specific to jobblaster search page.
*  ------------------------------------------------------*/
function clear_search_form(){
  $("#txt-keywords").val("");
  $("#txt-citystate").val("");
  $("#sel-distance").val("10000");
  $("#sel-category").val("");
  $("#sel-shift").val("");
  if(strFacility == ""){$("#sel-location").val("");}
}

function init_tabs(){
  var _rel = $("ul.tabs li a.active").attr("rel");
  $("." + _rel).show();
}

function set_tabs_click(){
  $("ul.tabs li a").click(function(){

    $(".active").removeClass("active");
    $(this).addClass("active");

    $("div.tab-content").hide();

    var _showTab = $(this).attr("rel");
    $("." + _showTab).show();

    if(_showTab == "map-tab"){
      initialize_map();
      load_map_locations();
        } //have to force reinitialization of google map.
        return false;
      });
}

function get_coords(){
  if (typeof read_location_cookie() === "undefined" || read_location_cookie() == ""){

    if(queryStringParser("do") != "search"){
            jb.log("Calling geolookup"); //TODO
            geo_lookup();
          }
        //geo_lookup();
      }else{
        //If cookie exists, server-side should take care of it.
        jb.log("Cookie exists, load jobs from server-side for "+ read_location_cookie() +".");
        if(queryStringParser("do") != "search"){
          var posit = jQuery.parseJSON(read_location_cookie());
          set_form_inputs(posit.city, posit.state, ApiRadius);
        }
      }
    }

    function init_results_table(){
      /*docs: legacy.datatables.net*/
      dataTable = $("#jb-search-results").dataTable({
        "bAutoWidth": false,
        "bProcessing": false,
        "iDisplayLength": 10,
        "sPaginationType": "full_numbers",
        "oLanguage":{
          "oPaginate":{"sNext":"Next &gt;", "sPrevious": "&lt; Prev", "sFirst":"&lt;&lt;", "sLast":"&gt;&gt;"},
          "sInfo": "Showing _START_ to _END_ (" + totalJobs + " jobs found)",
          "sZeroRecords": "No matching positions were found."
        },
        "aoColumnDefs": [{"bVisible":false,"aTargets":[0]}],
        "bFilter": false,
        "bLengthChange": false,
        "bRetrieve": true,
        "aaData": tblDataRows,
        "aoColumns": tblDataCols
      });
      dataTable.fnSort([[0,'asc']]);

      if(dataTable.fnGetData().length == 0){
        //console.log("no data found, hide paging and info.");
        $(".dataTables_paginate").css("display", "none");
        $("#jb-search-results_info").css("display", "none");
      }
    //"sInfo": "Showing _START_ to _END_ of _TOTAL_ positions." - prints to the bottom of the table "showing 0-xx of xxx positions."
    $("#jb-search-results_paginate").after("<div class='about-search-results'>Only the first 150 search results are shown. You can refine your search by adding keywords or using the filters.</div>");
  }

  function re_init_results_table(jsonData){
    dataTable.fnClearTable();
    dataTable.fnAddData(jsonData);
    dataTable.fnDraw();
  }

  function geo_lookup(){
    //initialize default location in case user refuses to share location or
    //browser does not support geolocation.
    userLoc.city = defaultCity;
    userLoc.state = defaultState;

    jb.log("city: '"+ userLoc.city +"', state: '"+ userLoc.state +"'");

    //request location if browser can handle it.
    if(geoPosition.init()){
      geoPosition.getCurrentPosition(set_current_position, geo_lookup_error, {enableHighAccuracy: true});
    }
  }

  function geo_lookup_error(lookupErr){
    //this really does nothing. We could set the default location here, but that's already done.
    //If debugging, print out info here.
    jb.log("Unable to query user location. Message returned as ["+ lookupErr +"].");
    jb.log("Using default location of: " + dCity + ", " + dState);
  }

  function set_current_position(positObj){
    //call webservice to get closest city-state combo, & jobs.
    var _query = "/jobsearch/?format=jsonp&lat="+ positObj.coords.latitude + "&lon=" + positObj.coords.longitude + "&limit="+ ApiLimit +"&radius=" + ApiRadius;

    jb.api(_query, "job_search_results");

    jb.log("Query from javascript (set_current_position): " + _query);
  }

  function job_search_results(wsData){

    //set_form_inputs(wsData.meta.city, wsData.meta.state, wsData.meta.radius);

    //jb.log("loading from javascript");
    jb.log("location retrieved from API is: "+ wsData.meta.city + ", " + wsData.meta.state);

    //$("#search-results").find("tr:gt(0)").remove();

    var objectCount = wsData.objects.length;
    var tableRows = "";

    for(var i = 0; i < objectCount; i++){
      var rows = '{';
      if(wsData.objects[i].distance){
        rows += '"distance":"'+wsData.objects[i].distance+'",';
      }else{
        rows += '"distance":"NULL",';
      }
      rows += '"title":"<a href=\'job.php?id='+wsData.objects[i].slug+'\' class=\'job-link\'>'+ RemoveUnsupportedStrings(wsData.objects[i].title) +'</a>",';
      rows += '"location":"<a href=\'job.php?id='+wsData.objects[i].slug+'\' class=\'job-link\'>'+ wsData.objects[i].facility + '<br>' + wsData.objects[i].city + ", " + wsData.objects[i].state +'</a>",';
      rows += '"shift":"<a href=\'job.php?id='+wsData.objects[i].slug+'\' class=\'job-link\'>'+wsData.objects[i].shift+'</a>"';
      rows += '},';
      tableRows += rows;
    }

    tableRows = '[' + removeTrailingComma(tableRows) +']';

    //jb.log("Table Rows (see line 310 approx): " + tableRows);     //TODO: comment out.

    //reinitialize the table sorting/filter functionality.
    re_init_results_table(JSON.parse(tableRows));

    set_user_location(wsData.meta.city, wsData.meta.state, wsData.meta.lat, wsData.meta.lon);

    get_facilities_nearby(wsData.meta.city, wsData.meta.state);

    //document.location.reload(true);
  }

  function set_user_location(_city, _state, _latitude, _longitude){
    userLoc.city = _city;
    userLoc.state = _state;
    userLoc.latitude = _latitude;
    userLoc.longitude = _longitude;

    set_location_cookie(userLoc.toString());

    set_form_inputs(_city, _state, ApiRadius);
  }

  function set_form_inputs(_city, _state, _radius){
    jb.log("Setting location and radius to:"+ _city +","+_state+" at radius " + _radius);

    $("#txt-citystate").val(_city + ", " + _state);

    if($.isNumeric(_radius)){
      if(parseInt(_radius) > 200){
        $("#sel-distance").val("10000");
      }else{
        $("#sel-distance").val(_radius);
      }
    }
  }

  function get_facilities_nearby(_city, _state){
    var _query = "/facilitysearch/?format=jsonp&loc=" + _city + "," + _state + "&limit=200";
    jb.api(_query, "facilities_nearby");
  }

  function facilities_nearby(wsData){
    if(wsData.objects.length > 0){

      var string_to_json = "";

      for(var j=0; j < wsData.objects.length; j++){
        string_to_json += '{"facility":"'+ wsData.objects[j].name +'","longitude":"'+ wsData.objects[j].location[0] +'","latitude":"'+ wsData.objects[j].location[1] +'","city":"'+ wsData.objects[j].city +'","state":"'+ wsData.objects[j].state +'","address":"'+ wsData.objects[j].address +'","zip":"'+ wsData.objects[j].zip_code +'"},';
      }

        //remove trailing comma.
        //string_to_json = string_to_json.replace(/,+$/, "");
        string_to_json = removeTrailingComma(string_to_json);

        //convert to json.
        string_to_json = '"values":['+ string_to_json +']';
        locations = JSON.parse('{'+string_to_json+'}');

        remove_markers();
        //load_map_locations();
      }
    }


/* -------------------------------------------------------
*   Utility functions.
*  ------------------------------------------------------*/

/**
 * html5AttributeSupport   Simple test for browser support
 *                         for a given HTML5 attribute in an
 *                         element.
 * @param  {String} theElement
 * @param  {String} theAttribute
 * @return {Boolean}
 */
 function html5AttributeSupport(theElement, theAttribute){
  var _testObject = document.createElement(theElement);
  var _isSupported = false;
  if(theAttribute in _testObject){
    _isSupported = true;
  }
  return _isSupported;
} // end html5AttributeSupport()

/**
 * init_text_placeholder Adds a 'placeholder' value to an HTML Input
 *                       text element if the browser supports it. Uses
 *                       the '<label>' for that element to add the placeholder
 *                       text. Note that the label can be set to display:none.
 *                       NOTE: there's a lot that's hardcoded here, so take
 *                       care when using.
 */
 function init_text_placeholder(){
  if(html5AttributeSupport("input", "placeholder")){
        //if browser supports placeholder, then add the placeholder
        //value to the placeholder.
        $("input.placeholderinput").each(function(){
          var el = $(this);
          var elLabelId = el.attr("id") + "-label";
          var phValue = $("#" + elLabelId).text();
          el.attr("placeholder", phValue);
        });
      }else{
        //if browser does not support placeholder (looking at you IE7!),
        //get the value of the placeholder, and put it in the value attribute.
        $("input.placeholderinput").each(function(){
          var el = $(this);
          var elLabelId = el.attr("id") + "-label";
          var phValue = $("#" + elLabelId).text();
          if(el.attr("value") == ""){
            el.attr("value",phValue);
            el.css("color", "#999");
          }
          el.focus(function(){
            if(this.value == phValue){
              this.value = "";
            }
          }).blur(function(){
            if(this.value ==""){
              this.value = phValue;
              el.css("color","#999");
            }
          });
        });

        //make sure that when the form submits, the placeholder values are removed.
        $("form.jb-search-form-widget").submit(function(){
          $("input.placeholderinput").each(function(){
            var el = $(this);
            var elLabelId = el.attr("id") + "-label";
            var phValue = $("#" + elLabelId).text();
            if(el.attr("value") == phValue){
              el.attr("value", "");
            }
          });
        });
      }
} // end init_text_placeholder()

function read_location_cookie(){
  return $.cookie(LocationCookieName);
    //return "home"; //TODO: fix
  }

  function set_location_cookie(cookieData){
    if(cookieData != ""){
      $.cookie(LocationCookieName, cookieData);
    }
  }

  function queryStringParser(p){
    var vars = [], hash;
    var q = document.URL.split('?')[1];
    if(q != undefined){
      q = q.split('&');
      for(var i = 0; i < q.length; i++){
        hash = q[i].split('=');
        vars.push(hash[1]);
        vars[hash[0]] = hash[1];
      }
    }
    return vars[p];
  }

  function removeTrailingComma(val){
    var retString = "";
    if(val != ""){
      retString = val.replace(/,+$/, "");
    }
    return retString;
  }

  function RemoveUnsupportedStrings(uncleanStr){
    var cleanStr = uncleanStr;

    cleanStr.replace("\\\\", "&#92;&#92;");
    cleanStr.replace("\\", "&#92;");
    return cleanStr;
  }
