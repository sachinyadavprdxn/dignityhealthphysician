$(document).ready(function () {

//    $(window).on('orientationchange', function () {
//    if ($('body').hasClass('home')) {
//      location.reload();
//    }
//
//  });


// Video lightbox Functionality
if ($('body').hasClass('home')) {
  $('a.watch-video').click(function(e){
   e.preventDefault();
   var URL = $(this).attr('href');
   $('#box, #mobile-box').attr('src',URL);
   $('.overlay, .iframe-section').fadeIn(100);
 });
}

$('.radiology a').click(function(e) {
 e.preventDefault();
 var URL = $(this).attr('href');
 $('#box, #mobile-box').attr('src',URL);
 $('.overlay, .iframe-section-radiology').fadeIn(100);
});

$('.admin-info-pharmacist a').click(function(e) {
  e.preventDefault();
  var URL = $(this).attr('href');
  $('#another-box, #mobile-box').attr('src',URL);
  $('.overlay, .iframe-section-pharm').fadeIn(100);
});


$('.close-button, .overlay').on('click', function() {
  $('.overlay, .iframe-section, .iframe-section-pharm, .iframe-section-radiology').fadeOut(100);
  $('#another-box, #mobile-box').removeAttr('src');
  $('#box, #mobile-box').removeAttr('src');
});


var browserMozilla = /mozilla/.test(navigator.userAgent.toLowerCase());
if (browserMozilla) {
 $('.explore-dignity #dropdown option').css('padding-left','24px');
}

// var browserSafari = /safari/.test(navigator.userAgent.toLowerCase());
// alert(navigator.appVersion.indexOf("Mac")!=-1);


$(window).on('orientationchange', function () {

  if ($('body').hasClass('meet-people')) {
    location.reload();
  }

});


if (!!navigator.userAgent.match(/Trident\/7\./)) {
  $('html').addClass('ie11');
}


$('#pullDropDownTitle').click(function (event) {
  $('.dropName').toggleClass('up-arrow');
  $('#dropDownContent').slideToggle();
});


if ($(window).width() > 1024) {
  $('.tabs').mouseenter(function (event) {
    $(this).children('h3').css('color', '#575c64');
    $(this).children('p').show();
  }).mouseleave(function (event) {
    $(this).children('h3').css('color', '#59B8E8');
    $(this).children('p').hide();
  });



  if (navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Mac') !== -1) {
    $('.nav-search').css('padding-left', '3.5%');
    $('.spotlight .content').css('padding-left', '7%');
    $('.video-click').css('left', '-90px');
    $('.home .close-button').css({
      top: '5px',
      right: '22px'
    });

    if (($(window).width() > 1301) && ($(window).width() < 1378)) {
     $('.giantLinks').css('width', '66%');
   }

  //  if ($(window).width() == 999){
  //   $('.giantLinks').css('width', '60%');
  // }

  if ($(window).width() == 1023){
    $('.career-paths .discover-nav ul').css('margin-right', '10%');
  }

  if ($(window).width() == 1481){
    $('.giantLinks').css('width', '68.5%');
  }

  if ($(window).width() == 1315){
    $('.giantLinks').css('width', '67%');
  }
}

var isMacLike = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
if (isMacLike) {
  var isFirefox = typeof InstallTrigger !== 'undefined';
  if (isFirefox) {
    $('.nav-search').css('padding-left', '3.5%');
    $('.spotlight .content').css('padding-left', '7%');
    $('.video-click').css('left', '-90px');
    $('#pullDropDownTitle').addClass('careers-learnmore');
    $('.close-button').css({
      top: '11px',
      right: '11px'
    });
    $('.home .close-button').css({
      top: '22px',
      right: '22px'
    });

  }
}

      // Accordion functionality
      $('.accordion-header > p').click(function (e) {
        if ($('body').hasClass('admin-fellowship')) {
          $(document).scrollTop(790);
        } else {
          $(document).scrollTop(710);
        }
        $('.accordion-content').slideUp();
        if ($(this).hasClass('down-arrow')) {
          $(this).removeClass('down-arrow');
        } else {
          $('.accordion-header > p').removeClass('down-arrow');
          $(this).addClass('down-arrow');
        }
        if ($(this).next().is(':hidden') === true) {
          $(this).next().slideDown();
          $('.accordion-content').not(this).removeClass('down-arrow');
        }
      });

    }

    if ($(window).width() <= 1024) {
      $('.tabs h3').click(function (e) {
        $('.tabs p').slideUp();
        if ($(this).hasClass('up-slide')) {
          $(this).removeClass('up-slide');
        } else {
          $('.tabs h3').removeClass('up-slide');
          $(this).addClass('up-slide');
        }
        if ($(this).next().is(':hidden') === true) {
          $(this).next().slideDown();
          $('.tabs p').not(this).removeClass('up-slide');
        }
      });

      // Accordion functionality
      $('.accordion-header > p').click(function (e) {
        if ($('body').hasClass('admin-fellowship')) {
          $(document).scrollTop(500);
        } else {
          $(document).scrollTop(300);
        }
        $('.accordion-content').slideUp();
        if ($(this).hasClass('down-arrow')) {
          $(this).removeClass('down-arrow');
        } else {
          $('.accordion-header > p').removeClass('down-arrow');
          $(this).addClass('down-arrow');
        }
        if ($(this).next().is(':hidden') === true) {
          $(this).next().slideDown();
          $('.accordion-content').not(this).removeClass('down-arrow');
        }
      });

    }

    $('.mobileMenuContainer').click(function (event) {
     event.preventDefault();
     $('.container').css('position', 'absolute');
     $('.nav').toggle('slide', function() {
       $('.container').removeAttr('style');
       $('.container').toggleClass('container-mob');
     });
     $(this).toggleClass('orange-back');
     $('.mobileMenuContainer a').toggleClass('hamburger')
     $('header').toggleClass('container-mob-header');

   });

// Placeholder fot IE
if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
  $('input[placeholder], textarea[placeholder]').each(function () {
    var input = $(this);
    $(input).val(input.attr('placeholder'));
    input.css('color', '#999');

    $(input).focus(function () {
      if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.css('color', '#000');
      }
    });

    $(input).blur(function () {
      if (input.val() == '' || input.val() == input.attr('placeholder')) {
        input.val(input.attr('placeholder'));
        input.css('color', '#999');
      }
      else {
        input.css('color', '#444')
      }
    });
    $(input).keypress(function () {
      input.css('color', '#000');
    });
  });
}

// Slider functionality
if ($('body').hasClass('home')) {
  $('#slider').flexslider({
    animation: 'slide',
    useCSS: false
  });
}

// if ($('body').is('.home, .search-more-jobs')) {
//   get_categories();
//   alert('hiii');
//   // value of current job category page the user is on.
//   // This is only populated if the user is on a page
//   // for a specific job type/role.

//   var pageCat = 'Nurse Educator, Wound Care, Travel RN, Telemetry, Surgical Services / PACU, Patient Lift / Mobility / Transport, Nursing - Registered Nurse, New Grad, Neonatal ICU, Med / Surg, LVN / LPN, Long Term Care / Skilled Nursing Unit, Infection Control, Float Pool, Advanced Practice';

//   // Name of the facility.
//   // This is only populated if on a facility website.
//   // e.g. "St. Joseph's Hospital and Medical Center"
//   var facilityName = '';

//   // A value to identify the facility (provided by CMS).
//   // This is only populated if on a facility website.
//   // e.g. "St-Josephs-Hospital-and-Medical-Center-AZ"
//   var facility_id = '';


//   // get the current openings.
//   get_current_openings(true, pageCat, facilityName, facility_id);

// }
});


$(window).on('load',function(){
  function equalHeight() {
   if ($(window).width() >= (960 - (window.innerWidth - $(window).width()))) {
    if($('.nursing-specialities').outerHeight() < ($('.right-sidebar').outerHeight())) {
      $('.nursing-specialities').outerHeight($('.right-sidebar').outerHeight() + 20);
    } else {
      $('.right-sidebar').outerHeight($('.nursing-specialities').outerHeight());
    }
  }
}

equalHeight();

if ($(window).width() <= 1024){
  var $heightContainer = $('.main-container').height();
  $('.nav').css('height', $heightContainer);
}

var selectedText = $('#sel-careerarea option:selected').text();
$('#selected-option-value').text(selectedText);

$('#sel-careerarea').on('change', function() {
  if ($('#selected-option-value').text($('#sel-careerarea option:selected').text()));
});

var newSelectedText = $('#dropdown option:selected').text();
$('#new-selected-option-value').text(newSelectedText);

$('#dropdown').on('change', function() {
  if ($('#new-selected-option-value').text($('#dropdown option:selected').text()));
});

var optionSelectedText = $('#sel-category option:selected').text();
$('#quality-option-value').text(optionSelectedText);

$('#sel-category').on('change', function() {
  if ($('#quality-option-value').text($('#sel-category option:selected').text()));
});

var optionSelectedText = $('#sel-distance option:selected').text();
$('#distance-option-value').text(optionSelectedText);

$('#sel-distance').on('change', function() {
  if ($('#distance-option-value').text($('#sel-distance option:selected').text()));
});

var optionSelectedText = $('#sel-location option:selected').text();
$('#facilities-option-value').text(optionSelectedText);

$('#sel-location').on('change', function() {
  if ($('#facilities-option-value').text($('#sel-location option:selected').text()));
});

var optionSelectedText = $('#sel-shift option:selected').text();
$('#shifts-option-value').text(optionSelectedText);

$('#sel-shift').on('change', function() {
  if ($('#shifts-option-value').text($('#sel-shift option:selected').text()));
});


});

$(window).resize(function(event) {

  if ($(window).width() < 960 ){
    var $heightContainer = $('.main-container').height();
    $('.nav').css('height', $heightContainer);
  } else {
    $('.nav').hide();
  }

  if (navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Mac') !== -1) {
    if (($(window).width() > 1301) && ($(window).width() < 1378)) {
     $('.giantLinks').css('width', '66%');
   }

  //  if ($(window).width() == 999){
  //   $('.giantLinks').css('width', '60%');
  // }

  if ($(window).width() == 1023) {
    $('.career-paths .discover-nav ul').css('margin-right', '10%');
  }

  if ($(window).width() == 1481) {
    $('.giantLinks').css('width', '68.5%');
  }

  if ($(window).width() == 1315) {
    $('.giantLinks').css('width', '67%');
  }
}
});









